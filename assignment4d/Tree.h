/* -*- C++ -*- */

/***************************************
Class: CS251
Assignment Number: 4c
Honor Pledge: I pledge that I have not recieved nor given help on this assignment.
***************************************/

#ifndef _TREE_H
#define _TREE_H

#include "Component_Node.h"
#include "Refcounter.h"
#include "Typedefs.h"

/**
 * @class Tree
 * @brief Defines a bridge to the node implementation that
 *        actually contains the data items for the tree.
 */
template <typename T>
class Tree
{
public:
  typedef Tree_Iterator_Impl iterator;

  /// Default ctor
  Tree ();

  /// Ctor that takes a Node<T> *.
  Tree (Component_Node<T> *root, bool count = false);

  // Copy ctor
  Tree (const Tree &t);

  /// Assignment operator
  void operator= (const Tree &t);

  /// Dtor
  ~Tree (void);

  /// Check if tree is null tree.
  bool is_null (void) const;

  /// Return the item in the tree.
  const T &item (void) const;

  /// Return the left child.
  Tree<T> left (void) const;

  /// Return the right child.
  Tree<T> right (void) const;

  /// Returns a traversal iterator to the beginning.
  iterator *create_iterator (const std::string &type);

  /// Interface to the Visitor pattern
  void accept (Visitor &v);

private:
  /// The underlying pointer to the implementation. These are
  /// reference counted.
  Refcounter <Component_Node<T> > root_;
};

#include "Tree.cpp"

#endif /* _TREE_H */
