/* -*- C++ -*- */

/***************************************
Class: CS251
Assignment Number: 4c
Honor Pledge: I pledge that I have not recieved nor given help on this assignment.
***************************************/

#if !defined (_TREE_ITERATOR_IMPL_CPP)
#define _TREE_ITERATOR_IMPL_CPP

#include <string>

#include "Component_Node.h"
#include "Print_Visitor.h"
#include "Tree.h"
#include "Options.h"
#include "Tree_Iterator_Impl.h"


// Destructor
Tree_Iterator_Impl::~Tree_Iterator_Impl()
{
}

// Constructor
Level_Order_Tree_Iterator_Impl::Level_Order_Tree_Iterator_Impl(const TREE &tree):
queue_()
{
  make_queue_strategy(Options::instance()->queue_type());
  if(!tree.is_null())
    queue_->enqueue(tree);

}

// Destructor
Level_Order_Tree_Iterator_Impl::~Level_Order_Tree_Iterator_Impl(void)
{
  // You fill in here.
}

// Dereferencing
TREE
Level_Order_Tree_Iterator_Impl::current_item (void)
{
  return queue_->front();
}

// Increment the iterator.
void
Level_Order_Tree_Iterator_Impl::advance (void)
{
    if(!done()){
		TREE tree = queue_->front();
		if(!(tree.left().is_null()))
			queue_->enqueue(tree.left());
		if(!(tree.right().is_null()))
			queue_->enqueue(tree.right());
		queue_->dequeue();
    }
}

bool
Level_Order_Tree_Iterator_Impl::done (void)
{
	return queue_->is_empty();
}

// Constructor
Pre_Order_Tree_Iterator_Impl::Pre_Order_Tree_Iterator_Impl(const TREE &tree):
  stack_()
{
  if(!tree.is_null()){
	  stack_.push(tree);
  }
}

// Destructor
Pre_Order_Tree_Iterator_Impl::~Pre_Order_Tree_Iterator_Impl(void)
{
  // You fill in here.
}

// Dereferencing
TREE
Pre_Order_Tree_Iterator_Impl::current_item (void)
{
  return stack_.top();
}

// Increment the iterator
void
Pre_Order_Tree_Iterator_Impl::advance (void)
{
  if(!done()){
	  TREE tree = stack_.top();
	  stack_.pop();
	  //reversed
	  if(!tree.right().is_null()){
		  stack_.push(tree.right());
	  }
	  if(!tree.left().is_null()){
		  stack_.push(tree.left());
	  }

  }
}

// Check if the iterator is done.
bool
Pre_Order_Tree_Iterator_Impl::done (void)
{
  return stack_.empty();
}

void
Level_Order_Tree_Iterator_Impl::make_queue_strategy (const std::string &queue_type)
{
    if(queue_type == "AQueue"){
	   queue_ = std::unique_ptr<QUEUE>(new AQUEUE_ADAPTER(80));
	   return;
    }
    if(queue_type == "LQueue"){
	   queue_ = std::unique_ptr<QUEUE>(new LQUEUE_ADAPTER(0));
      return;
    }
    throw Unknown_Strategy(queue_type);
}

#endif /* _TREE_ITERATOR_IMPL_CPP */
