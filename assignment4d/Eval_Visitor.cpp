#if !defined (_Eval_Visitor_CPP)
#define _Eval_Visitor_CPP

#include "Component_Node.h"
#include "Leaf_Node.h"
#include "Composite_Negate_Node.h"
#include "Composite_Add_Node.h"
#include "Composite_Subtract_Node.h"
#include "Composite_Divide_Node.h"
#include "Composite_Multiply_Node.h"
#include "Tree.h"

/// Ctor
template <typename T>
Post_Order_Eval_Visitor<T>::Post_Order_Eval_Visitor (void)
{
  // You fill in here.
}

/// Dtor
template <typename T>
Post_Order_Eval_Visitor<T>::~Post_Order_Eval_Visitor (void)
{
  // You fill in here.
}

/// visit method for Leaf_Node<int> instance
template <typename T> void
Post_Order_Eval_Visitor<T>::visit (const LEAF_NODE& node)
{
  stack_.push(node.item());
}

/// visit method for Composite_Negate_Node<int> instance
template <typename T> void
Post_Order_Eval_Visitor<T>::visit (const COMPOSITE_NEGATE_NODE& node)
{
	if(stack_.empty())
		throw std::runtime_error("Stack empty");
	T temp = stack_.top();
	temp = temp*-1;
	stack_.pop();
	stack_.push(temp);
}

/// visit method for Composite_Add_Node<int> instance
template <typename T> void
Post_Order_Eval_Visitor<T>::visit (const COMPOSITE_ADD_NODE& node)
{
        // @@ for all these operations, if the stack is empty, you need to throw an exception.
	//changed
	if(stack_.empty())
		throw std::runtime_error("Stack empty");
	T left = stack_.top();
	stack_.pop();
	if(stack_.empty())
		throw std::runtime_error("Empty Stack");
	T right = stack_.top();
	stack_.pop();

	stack_.push(right + left);
}

/// visit method for Composite_Subtract_Node<int> instance
template <typename T> void
Post_Order_Eval_Visitor<T>::visit (const COMPOSITE_SUBTRACT_NODE& node)
{
	//changed
	if(stack_.empty())
		throw std::runtime_error("Empty Stack");
	T left = stack_.top();
	stack_.pop();
	if(stack_.empty())
		throw std::runtime_error("Empty Stack");
	T right = stack_.top();
	stack_.pop();

	stack_.push(right - left);
}

/// visit method for Composite_Multiply_Node<int> instance
template <typename T> void
Post_Order_Eval_Visitor<T>::visit (const COMPOSITE_MULTIPLY_NODE& node)
{
	if(stack_.empty())
		throw std::runtime_error("Empty Stack");
	T left = stack_.top();
	stack_.pop();

	if(stack_.empty())
		throw std::runtime_error("Empty Stack");
	T right = stack_.top();
	stack_.pop();

	stack_.push(right * left);
}
/// visit method for Composite_Divide_Node<int> instance
template <typename T> void
Post_Order_Eval_Visitor<T>::visit (const COMPOSITE_DIVIDE_NODE& node)
{

	if(stack_.empty()) throw std::runtime_error("Empty Stack");
	T left = stack_.top();
	stack_.pop();

	if(stack_.empty()) throw std::runtime_error("Empty Stack");
	T right = stack_.top();
	stack_.pop();
    // @@ You need to check for a divide by zero and throw an exception!
	if(left == 0) throw std::runtime_error("Divide by zero");
	stack_.push(right / left);
}

// yields the output of the visit
template <typename T> T
Post_Order_Eval_Visitor<T>::yield (void)
{
  return stack_.top();
}

/// Ctor
template <typename T>
Pre_Order_Eval_Visitor<T>::Pre_Order_Eval_Visitor (void)
{
}

/// Dtor
template <typename T>
Pre_Order_Eval_Visitor<T>::~Pre_Order_Eval_Visitor (void)
{
}

/// visit method for Composite_Subtract_Node<int> instance
template <typename T> void
Pre_Order_Eval_Visitor<T>::visit (const COMPOSITE_SUBTRACT_NODE& node)
{
	if(Post_Order_Eval_Visitor<T>::stack_.empty())
		throw std::runtime_error("Empty Stack");
	T left = Post_Order_Eval_Visitor<T>::stack_.top();
	Post_Order_Eval_Visitor<T>::stack_.pop();

	if(Post_Order_Eval_Visitor<T>::stack_.empty())
			throw std::runtime_error("Empty Stack");
	T right = Post_Order_Eval_Visitor<T>::stack_.top();
	Post_Order_Eval_Visitor<T>::stack_.pop();

	Post_Order_Eval_Visitor<T>::stack_.push(left - right);
}


/// visit method for Composite_Divide_Node<int> instance
template <typename T> void
Pre_Order_Eval_Visitor<T>::visit (const COMPOSITE_DIVIDE_NODE& node)
{

	if(Post_Order_Eval_Visitor<T>::stack_.empty())
			throw std::runtime_error("Empty Stack");
	T left = Post_Order_Eval_Visitor<T>::stack_.top();
	Post_Order_Eval_Visitor<T>::stack_.pop();

	if(Post_Order_Eval_Visitor<T>::stack_.empty())
			throw std::runtime_error("Empty Stack");
	T right = Post_Order_Eval_Visitor<T>::stack_.top();
	Post_Order_Eval_Visitor<T>::stack_.pop();
          // @@ You need to check for a divide by zero and throw an exception!
	if(right == 0)
		throw std::runtime_error("Divide by zero");
	Post_Order_Eval_Visitor<T>::stack_.push(left/right);

}

#endif /* _Eval_Visitor_CPP */
