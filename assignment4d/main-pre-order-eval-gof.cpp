#include <iostream>
#include <memory>
#include <algorithm>
#include <string>
#include <stack>

#include "Tree.h"
#include "Factories.h"
#include "Options.h"
#include "Eval_Visitor.h"

int 
main (int argc, char *argv[])
{
  try 
    {
      // Create options class to parse command line options.
      std::auto_ptr<Options> options (Options::instance ());

      // Parse the command-line options. If the user requested help (via "-h"
      // or "-?") then print out the usage and return.
      if (!Options::instance ()->parse_args (argc, argv))
        return 0;

      // Use the factory to create the tree.
      TREE root_node = make_tree ();

      std::cout << "Testing the Print_Visitor: " << std::endl;

      std::stack<TREE> reverse_pre_order;

      for (std::auto_ptr<TREE::iterator> iter (root_node.create_iterator ("Preorder"));
           !iter->done ();
           iter->advance ())
        reverse_pre_order.push (iter->current_item ());

      Pre_Order_Eval_Visitor<int> eval_visitor;
      Print_Visitor print_visitor;

      while (reverse_pre_order.size () > 0)
        {
          TREE t = reverse_pre_order.top ();
          t.accept (eval_visitor);
          t.accept (print_visitor);
          reverse_pre_order.pop ();
        }

      std::cout << std::endl << "yield of the tree = " << eval_visitor.yield () << std::endl;

    }
  catch (...)
    {
      std::cout << "some exception occurred" << std::endl;
    }

  return 0;
}
