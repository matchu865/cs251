/* -*- C++ -*- */

/***************************************
Class: CS251
Assignment Number: 4b
Honor Pledge: I pledge that I have not recieved nor given help on this assignment.
***************************************/

#if !defined (_Factories_CPP)
#define _Factories_CPP

#include <string>

#include "Factories.h"
#include "Options.h"
#include "Component_Node.h"
#include "Composite_Add_Node.h"
#include "Composite_Binary_Node.h"
#include "Composite_Divide_Node.h"
#include "Composite_Multiply_Node.h"
#include "Composite_Negate_Node.h"
#include "Composite_Subtract_Node.h"
#include "Composite_Unary_Node.h"
#include "Leaf_Node.h"

// Method to create the binary tree to traverse.
TREE
make_tree (void)
{
  //             +
  //        /         \
  //      -             *
  //    /   \         /   \
  //  3       4     7       5
  // 
  // Make/return a balanced expression tree that matches what's shown above.
	  COMPONENT_NODE *t1 = new LEAF_NODE (3);
	  COMPONENT_NODE *t2 = new LEAF_NODE (4);
	  COMPONENT_NODE *t3 = new COMPOSITE_SUBTRACT_NODE (t1, t2);
	  COMPONENT_NODE *t4 = new LEAF_NODE (7);
	  COMPONENT_NODE *t5 = new LEAF_NODE (5);
	  COMPONENT_NODE *t6 = new COMPOSITE_MULTIPLY_NODE (t4, t5);
	  COMPONENT_NODE *t7 = new COMPOSITE_ADD_NODE(t3,t6);
	  return TREE(t7);

}

#endif /* _Factories_CPP */
