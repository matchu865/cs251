#include "LQueue.h"
#include "gtest/gtest.h"
#include <algorithm>
#include <vector>
#include <chrono>
#include <random>

#ifdef ENABLE_LQUEUE_TESTS

typedef LQueue<int> AQ;

namespace {
std::default_random_engine
generator(std::chrono::system_clock::now().time_since_epoch().count());
std::uniform_int_distribution<int> distribution(0, 100);
}

#define RANDOM_POPULATE(TEMP_NAME, SUBJECT, LENGTH)                            \
  std::vector<AQ::value_type> TEMP_NAME(LENGTH);                               \
  std::generate(TEMP_NAME.begin(), TEMP_NAME.end(),                            \
                std::bind(distribution, generator));                           \
                                                                               \
  std::for_each(TEMP_NAME.begin(), TEMP_NAME.end(),                            \
                [&SUBJECT, TEMP_NAME](const AQ::value_type &item) {            \
    SUBJECT.enqueue(item);                                                     \
  })


TEST(LQConstructor, SizeT) {
    AQ q(20);
    const AQ q1(20);
    EXPECT_EQ(0u, q.size());
    EXPECT_TRUE(q.is_empty());
    EXPECT_FALSE(q.is_full());
    EXPECT_EQ(q.begin(), q.end());
    EXPECT_EQ(q1.begin(), q1.end());
}

TEST(LQConstructor, CopyNonEmpty) {
    AQ a(10);
    for (size_t i = 0; i < 10; ++i)
        a.enqueue(i);

    AQ b(a);
    const AQ c(a);
    EXPECT_EQ(10u, b.size());
    EXPECT_FALSE(b.is_empty());
    EXPECT_FALSE(b.is_full());
    EXPECT_NE(b.begin(), b.end());
    EXPECT_NE(c.begin(), c.end());
    EXPECT_NE(b.begin(), a.begin());
    EXPECT_EQ(a, b);
}

TEST(LQAssignment, VisualCheckForSelfAssignmentCheckAndCopyAndSwap) {}

//bool operator==(const std::vector<int>& v, const AQ& q) {
//    return v.size() == q.size() && std::equal(v.cbegin(), v.cend(), q.cbegin());
//}

/*
bool operator==(const std::vector<int>& v, const AQ& q) {
    return v.size() == q.size() && std::equal(v.cbegin(), v.cend(), q.begin());
}
*/

/*
NEEDS THE ABOVE == OPERATOR

TEST(LQEnqueue, Simple) {
    std::vector<int> values;
    AQ q(10);  //Was for Default Constructor, but I made it 10
    for (size_t i = 0; i < 10; ++i) {
        EXPECT_EQ(i, q.size());
        EXPECT_EQ(values, q);   //Not sure about this

        q.enqueue(i);
        values.push_back(i);
        EXPECT_FALSE(q.is_empty());
    }
}
*/


TEST(LQDequeue, Empty) {
    AQ q(1);  //Was for Default Constructor, but I made it 1
    EXPECT_EQ(0u, q.size());
    EXPECT_TRUE(q.is_empty());
    EXPECT_THROW(q.dequeue(), AQ::Underflow);
}

TEST(LQDequeue, NonEmpty) {
    AQ q(10);   //Was for Default Constructor, but I made it 10. Should I have made it 1 or 0
    for (int i = 1; i <= 10; ++i) {
        q.enqueue(i);
        ASSERT_EQ((size_t) i, q.size());
        ASSERT_FALSE(q.is_empty());
    }

    for (int i = 1; i <= 10; ++i) {
        ASSERT_EQ(11u - i, q.size());
        ASSERT_EQ(i, q.front());
        ASSERT_FALSE(q.is_empty());
        q.dequeue();
    }

    ASSERT_EQ(0u, q.size());
    ASSERT_TRUE(q.is_empty());
    EXPECT_THROW(q.dequeue(), AQ::Underflow);
}

TEST(LQFront, Empty) {
    AQ q(1);  //Was for Default Constructor, but I made it 10
    EXPECT_THROW(q.front(), AQ::Underflow);
}

TEST(LQFront, NonEmpty) {
    AQ q(10);

    for (int i = 1; i <= 10; ++i) {
      q.enqueue(i);
    }

    for (int i = 1; i <= 10; ++i) {
      EXPECT_EQ(i, q.front());
      q.dequeue();
    }


    ASSERT_EQ(0u, q.size());
    ASSERT_TRUE(q.is_empty());
    EXPECT_THROW(q.front(), AQ::Underflow);
}

TEST(LQEquality, Empty) {
    AQ a(0), b(0);  //Was for Default Constructor, but I made it 0. Should it be 1
    EXPECT_EQ(a, b);
    EXPECT_EQ(b, a);
}

TEST(LQEquality, EmptyToNonEmpty) {
    AQ empty(10), nonEmpty(10); //Was for Default Constructor, but I made it 10
    for (int i = 0; i < 10; ++i)
        nonEmpty.enqueue(i);
    EXPECT_NE(empty, nonEmpty);
    EXPECT_NE(nonEmpty, empty);
}

TEST(LQEquality, Self) {
    AQ q(10);  //Was for Default Constructor, but I made it 10
    for (int i = 0; i < 10; ++i) {
        EXPECT_EQ(q, q);
        q.enqueue(i);
    }
}

TEST(LQEquality, Equal) {
    AQ a(10), b(10);  //Was for Default Constructor, but I made it 10
    for (int i = 0; i < 10; ++i) {
        EXPECT_EQ(a, b);
        EXPECT_EQ(b, a);
        a.enqueue(i);
        b.enqueue(i);
    }
}

TEST(LQEquality, CommonPrefixDifferentSizes) {
    AQ shorter(20);
    for (int i = 0; i < 10; ++i)
        shorter.enqueue(i);
    AQ longer(shorter);
    for (int i = 10; i < 20; ++i)
        longer.enqueue(i);

    EXPECT_NE(shorter, longer);
    EXPECT_NE(longer, shorter);
}

TEST(LQIterators, PrefixIncrement) {
     AQ q(10);
  RANDOM_POPULATE(temp_array, q, 10);
  int j = 0;

  for (AQ::iterator i = q.begin(); i != q.end() && j++ < 10; ++i) {
    EXPECT_EQ(temp_array[j - 1], *i);

    // To see the values in the queue, uncomment the line below
    // std::cout << "i = " << *i << " should be equal to " << temp_array[j-1] <<
    // std::endl;
  }

  // Pre-Operator ++
  j = 0;

  for (AQ::iterator i = q.begin(); i != q.end() && j < 9; ++j) {
    EXPECT_EQ(temp_array[j + 1], *++i);
  }

}

TEST(LQIterators, PostfixIncrement) {
  AQ q(10);

  RANDOM_POPULATE(temp_array, q, 10);

  // Post-Operator ++
  int j = 0;
  for (AQ::iterator i = q.begin(); i != q.end() && j < 10; ++j) {
    EXPECT_EQ(temp_array[j], *i++);
  }
}

#if GRADUATE_STUDENT == 1

TEST(LQIterators, PrefixDecrement) {
  AQ q(10);

  RANDOM_POPULATE(temp_array, q, 10);
  // Pre-Operator --

  int j = 9;
  for (AQ::iterator i = q.end(); i != q.begin() && j >= 0; --j) {
    EXPECT_EQ(temp_array[j], *--i);
  }
}

TEST(LQIterators, PostfixDecrement) {
  AQ q(10);

  RANDOM_POPULATE(temp_array, q, 10);

  // Post-Operator --
  int j = 10;
  for (AQ::iterator i = --(q.end()); i != q.begin() && j > 1; --j) {
    EXPECT_EQ(temp_array[j - 1], *i--);
  }
}
#endif

TEST(LQIterators, EqualityOperator) {
   AQ q(10);
  RANDOM_POPULATE(temp_array, q, 10);
  AQ r(q);

  auto iqb = q.begin();
  auto iqe = q.end();
  auto irb = r.begin();
  auto ire = r.end();

  EXPECT_NE(q.begin(), q.end());
  EXPECT_EQ(q.begin(), q.begin());
  EXPECT_EQ(q.end(), q.end());

  EXPECT_EQ(iqb, q.begin());
  EXPECT_NE(iqb, q.end());
  EXPECT_EQ(iqe, q.end());
  EXPECT_NE(iqe, q.begin());

  EXPECT_NE(q.begin(), r.begin());
  EXPECT_NE(iqb, ire);
  EXPECT_NE(iqb, irb);
}

#if 0
TEST(LQIterators, InequalityOperator) {
  AQ q(10);

  RANDOM_POPULATE(temp_array, q, 10);

  // Checking operator !=

  AQ::iterator temp_itr1 = q.begin();
  int j = 0;
  for (AQ::iterator i = q.begin(); i != q.end(); ++j) {
    if (j == 0) {
      EXPECT_EQ(*(temp_itr1), *i++);
    } else {
      EXPECT_NE(*(temp_itr1), *i++);
    }
  }
}
#endif

TEST(LQConstantIterators, PrefixIncrement) {
  AQ q1(10);

  RANDOM_POPULATE(temp_array, q1, 10);

  const AQ q(q1);

  int j = 0;

  for (AQ::const_iterator i = q.begin(); i != q.end() && j++ < 10; ++i) {
    EXPECT_EQ(temp_array[j - 1], *i);

    // To see the values in the queue, uncomment the line below
    // std::cout << "i = " << *i << " should be equal to " << temp_array[j-1] <<
    // std::endl;
  }

  // Pre-Operator ++
  j = 0;

  for (AQ::const_iterator i = q.begin(); i != q.end() && j < 9; ++j) {
    EXPECT_EQ(temp_array[j + 1], *++i);
  }

}

TEST(LQConstantIterators, PostfixIncrement) {
  AQ q1(10);

  RANDOM_POPULATE(temp_array, q1, 10);

  const AQ q(q1);

  // Post-Operator ++
  int j = 0;
  for (AQ::const_iterator i = q.begin(); i != q.end() && j < 10; ++j) {
    EXPECT_EQ(temp_array[j], *i++);
  }
}

#if GRADUATE_STUDENT == 1

TEST(LQConstantIterators, PrefixDecrement) {
  AQ q1(10);

  RANDOM_POPULATE(temp_array, q1, 10);

  const AQ q(q1);

  // Pre-Operator --

  int j = 9;
  for (AQ::const_iterator i = q.end(); i != q.begin() && j >= 0; --j) {
    EXPECT_EQ(temp_array[j], *--i);
  }
}

TEST(LQConstantIterators, PostfixDecrement) {
  AQ q1(10);

  RANDOM_POPULATE(temp_array, q1, 10);

  const AQ q(q1);

  // Post-Operator --
  int j = 10;
  for (AQ::const_iterator i = --(q.end()); i != q.begin() && j > 1; --j) {
    EXPECT_EQ(temp_array[j - 1], *i--);
  }
}
#endif

TEST(LQConstantIterators, EqualityOperator) {
  AQ oq(10);
  RANDOM_POPULATE(temp_array, oq, 10);
  const AQ r(oq), q(oq);

  auto iqb = q.begin();
  auto iqe = q.end();
  auto irb = r.begin();
  auto ire = r.end();

  EXPECT_NE(q.begin(), q.end());
  EXPECT_EQ(q.begin(), q.begin());
  EXPECT_EQ(q.end(), q.end());

  EXPECT_EQ(iqb, q.begin());
  EXPECT_NE(iqb, q.end());
  EXPECT_EQ(iqe, q.end());
  EXPECT_NE(iqe, q.begin());

  EXPECT_NE(q.begin(), r.begin());
  EXPECT_NE(iqb, ire);
  EXPECT_NE(iqb, irb);
}

#if 0
TEST(LQConstantIterators, InequalityOperator) {
  AQ q1(10);

  RANDOM_POPULATE(temp_array, q1, 10);

  const AQ q(q1);

  // Checking operator !=

  AQ::const_iterator temp_itr1 = q.begin();
  int j = 0;
  for (AQ::const_iterator i = q.begin(); i != q.end(); ++j) {
    if (j == 0) {
      EXPECT_EQ(*(temp_itr1), *i++);
    } else {
      EXPECT_NE(*(temp_itr1), *i++);
    }
  }
}
#endif

TEST(LQDequeueEnqueue, RandomNumbers) {

  AQ q(10);
  RANDOM_POPULATE(temp_array, q, 10);
  // Dequeueing

  for (int j = 0; j < 10; j++) {
    EXPECT_EQ(temp_array[j], q.front());
    q.dequeue();

  }
}

// TEST(LQSwap, VisualCheckForSimpleSwaps) {
// }

#if GRADUATE_STUDENT == 1
TEST(LQReverseIterators, PrefixIncrement) {
  AQ q(10);
  RANDOM_POPULATE(temp_array, q, 10);
  int j = 10;

  for (AQ::reverse_iterator i = q.rbegin(); i != q.rend() && j > 0; ++i) {
    EXPECT_EQ(temp_array[--j], *i);

    // To see the values in the queue, uncomment the line below
    // std::cout << "i = " << *i << " should be equal to " << temp_array[j-1] <<
    // std::endl;
  }

  // Pre-Operator ++
  j = 9;

  for (AQ::reverse_iterator i = q.rbegin(); i != q.rend() && j > 0; ) {
    EXPECT_EQ(temp_array[--j], *++i);
  }
}

TEST(LQReverseIterators, PostfixIncrement) {
  AQ q(10);

  RANDOM_POPULATE(temp_array, q, 10);

  // Post-Operator ++
  int j = 9;
  for (AQ::reverse_iterator i = q.rbegin(); i != q.rend() && j > 0; --j) {
    EXPECT_EQ(temp_array[j], *i++);
  }
}


TEST(LQReverseIterators, PrefixDecrement) {
  AQ q(10);

  RANDOM_POPULATE(temp_array, q, 10);
  // Pre-Operator --

  int j = 0;
  for (AQ::reverse_iterator i = q.rend(); i != q.rbegin() && j < 10; ++j) {
    EXPECT_EQ(temp_array[j], *--i);
  }
}

TEST(LQReverseIterators, PostfixDecrement) {
  AQ q(10);

  RANDOM_POPULATE(temp_array, q, 10);

  // Post-Operator --
  int j = 0;
  for (AQ::reverse_iterator i = --(q.rend()); i != q.rbegin() && j < 9; ++j) {
    EXPECT_EQ(temp_array[j], *i--);
  }
}


TEST(LQReverseIterators, EqualityOperator) {
  AQ q(10);
  RANDOM_POPULATE(temp_array, q, 10);
  AQ r(q);

  auto iqrb = q.rbegin();
  auto iqre = q.rend();
  auto irrb = r.rbegin();
  auto irre = r.rend();

  EXPECT_NE(q.rbegin(), q.rend());
  EXPECT_EQ(q.rbegin(), q.rbegin());
  EXPECT_EQ(q.rend(), q.rend());

  EXPECT_EQ(iqrb, q.rbegin());
  EXPECT_NE(iqrb, q.rend());
  EXPECT_EQ(iqre, q.rend());
  EXPECT_NE(iqre, q.rbegin());

  EXPECT_NE(q.rbegin(), r.rbegin());
  EXPECT_NE(iqrb, irre);
  EXPECT_NE(iqrb, irrb);
}

#if 0
TEST(LQReverseIterators, InequalityOperator) {
  AQ q(10);

  RANDOM_POPULATE(temp_array, q, 10);

  // Checking operator !=

  AQ::reverse_iterator temp_itr1 = q.rbegin();
  int j = 0;
  for (AQ::reverse_iterator i = q.rbegin(); i != q.rend(); ++j) {
    if (j == 0) {
      EXPECT_EQ(*(temp_itr1), *i++);
    } else {
      EXPECT_NE(*(temp_itr1), *i++);
    }
  }
}
#endif

TEST(LQConstantReverseIterators, PrefixIncrement) {
  AQ q1(10);
  RANDOM_POPULATE(temp_array, q1, 10);

  const AQ q(q1);

  int j = 10;

  for (AQ::const_reverse_iterator i = q.rbegin(); i != q.rend() && j > 0; ++i) {
    EXPECT_EQ(temp_array[--j], *i);

    // To see the values in the queue, uncomment the line below
    // std::cout << "i = " << *i << " should be equal to " << temp_array[j-1] <<
    // std::endl;
  }

  // Pre-Operator ++
  j = 9;

  for (AQ::const_reverse_iterator i = q.rbegin(); i != q.rend() && j > 0; ) {
    EXPECT_EQ(temp_array[--j], *++i);
  }
}

TEST(LQConstantReverseIterators, PostfixIncrement) {
  AQ q1(10);

  RANDOM_POPULATE(temp_array, q1, 10);

  const AQ q(q1);

  // Post-Operator ++
  int j = 9;
  for (AQ::const_reverse_iterator i = q.rbegin(); i != q.rend() && j > 0; --j) {
    EXPECT_EQ(temp_array[j], *i++);
  }
}


TEST(LQConstantReverseIterators, PrefixDecrement) {
  AQ q1(10);

  RANDOM_POPULATE(temp_array, q1, 10);

  const AQ q(q1);

  // Pre-Operator --

  int j = 0;
  for (AQ::const_reverse_iterator i = q.rend(); i != q.rbegin() && j < 10; ++j) {
    EXPECT_EQ(temp_array[j], *--i);
  }
}

TEST(LQConstantReverseIterators, PostfixDecrement) {
  AQ q1(10);

  RANDOM_POPULATE(temp_array, q1, 10);
  const AQ q(q1);
  // Post-Operator --
  int j = 0;
  for (AQ::const_reverse_iterator i = --(q.rend()); i != q.rbegin() && j < 9; ++j) {
    EXPECT_EQ(temp_array[j], *i--);
  }
}


TEST(LQConstantReverseIterators, EqualityOperator) {
  AQ q1(10);
  RANDOM_POPULATE(temp_array, q1, 10);

  const AQ q(q1);
  const AQ r(q1);

  auto iqrb = q.rbegin();
  auto iqre = q.rend();
  auto irrb = r.rbegin();
  auto irre = r.rend();

  EXPECT_NE(q.rbegin(), q.rend());
  EXPECT_EQ(q.rbegin(), q.rbegin());
  EXPECT_EQ(q.rend(), q.rend());

  EXPECT_EQ(iqrb, q.rbegin());
  EXPECT_NE(iqrb, q.rend());
  EXPECT_EQ(iqre, q.rend());
  EXPECT_NE(iqre, q.rbegin());

  EXPECT_NE(q.rbegin(), r.rbegin());
  EXPECT_NE(iqrb, irre);
  EXPECT_NE(iqrb, irrb);
}

#if 0
TEST(LQConstantReverseIterators, InequalityOperator) {
  AQ q(10);
  const AQ q(q1);

  RANDOM_POPULATE(temp_array, q, 10);

  // Checking operator !=

  AQ::const_reverse_iterator temp_itr1 = q.rbegin();
  int j = 0;
  for (AQ::const_reverse_iterator i = q.rbegin(); i != q.rend(); ++j) {
    if (j == 0) {
      EXPECT_EQ(*(temp_itr1), *i++);
    } else {
      EXPECT_NE(*(temp_itr1), *i++);
    }
  }
}
#endif
#endif

#else // ENABLE_LQUEUE_TESTS
#pragma message("If you would like the LQueue tests to be run, re-compile with \"#define ENABLE_LQUEUE_TESTS\" in your LQueue.h header file")
#endif
