/***************************************
Class: CS251
Assignment Number: 2
Honor Pledge: I pledge that I have not recieved nor given help on this assignment.
***************************************/

// Returns the current size of the array.

template <typename T> INLINE size_t 
Array<T>::size (void) const
{
	return cur_size_;
}

template <typename T> INLINE bool
Array<T>::in_range (size_t index) const
{
	return index < cur_size_;
}

template <typename T> INLINE T &
Array<T>::operator[] (size_t index)
{
	return array_[index];
}

template <typename T> INLINE const T &
Array<T>::operator[] (size_t index) const
{
	return array_[index];
}

