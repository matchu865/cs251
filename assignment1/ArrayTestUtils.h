#ifndef _ARRAY_TEST_UTILS_H_
#define _ARRAY_TEST_UTILS_H_

#define GRADUATE_STUDENT 0

#include "Array.h"
#include "gtest/gtest.h"
#include <vector>
#include <iostream>
#include <algorithm>

typedef std::vector<char> vector;

std::ostream& operator<<(std::ostream&, const Array&);

void PrintTo(const Array&, std::ostream*);

std::ostream& operator<<(std::ostream&, const vector&);

testing::AssertionResult equalWithIterators(const vector&, const Array&);

testing::AssertionResult equalWithGet(const vector&, const Array&);

testing::AssertionResult equalWithSubscript(const vector&, const Array&);

char* begin(Array& a);

char* end(Array& a);

const char* cbegin(const Array& a);

const char* cend(const Array& a);

#endif
