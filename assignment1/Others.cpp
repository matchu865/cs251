#include "ArrayTestUtils.h"
#include <stdexcept>

const vector tenZeros(10, '0');
const vector zeroNineVec{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

TEST(SetAndSubscript, Set) {
    Array a(10, '0');
    EXPECT_PRED2(equalWithGet, tenZeros, a);

    for (size_t i = 0; i < 10u; ++i)
        a.set(i + '0', i);
    EXPECT_PRED2(equalWithGet, zeroNineVec, a);

#if GRADUATE_STUDENT == 0
    EXPECT_EQ(-1, a.set('0', 10));
#else
    EXPECT_THROW(a.set('0', 10), std::out_of_range);
#endif
}

TEST(SetAndSubscript, Subscript) {
    Array a(10, '0');
    EXPECT_PRED2(equalWithSubscript, tenZeros, a);

    for (size_t i = 0; i < 10u; ++i)
        a[i] = i + '0';
    EXPECT_PRED2(equalWithSubscript, zeroNineVec, a);
}

TEST(EdgeConditions, SequentialStorage) {
    Array a(2);
    EXPECT_EQ(1, &a[1] - &a[0]);
}

TEST(EdgeConditions, AssignmentOptimization) {
    Array a(10), b(20);
    char* begin = &b[0];
    b = a;
    EXPECT_EQ(begin, &b[0]);
}

TEST(EdgeConditions, AssignmentChaining) {
    Array a(10), b(20), c(30);
    Array* aPtr = &a;
    EXPECT_EQ(aPtr, &(a = b = c));
}

TEST(EdgeConditions, GetFailsCorrectly) {
    Array a(10);

    char shouldFail;
#if GRADUATE_STUDENT == 0
    EXPECT_EQ(-1, a.get(shouldFail, 10));
#else
    EXPECT_THROW(a.get(shouldFail, 10), std::out_of_range);
#endif
}

