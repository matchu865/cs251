#include "ArrayTestUtils.h"

std::ostream& operator<<(std::ostream& os, const Array& a) {
    os << "[" << a.size() << "] { ";
    std::copy(cbegin(a), cend(a), std::ostream_iterator<char>(os, " "));
    os << "}";
    return os;
}

void PrintTo(const Array& a, std::ostream* os) {
    *os << a;
}

std::ostream& operator<<(std::ostream& os, const vector& v) {
    os << "[" << v.size() << "] { ";
    std::copy(v.cbegin(), v.cend(), std::ostream_iterator<char>(os, " "));
    os << "}";
    return os;
}

testing::AssertionResult equalWithIterators(const vector& v, const Array& a) {
    if (a.size() != v.size())
        return testing::AssertionFailure()
             << "size mismatch " << v << " vs " << a;

    if (std::equal(cbegin(a), cend(a), v.cbegin()))
        return testing::AssertionSuccess();

    return testing::AssertionFailure() << "value mismatch " << v << " vs " << a;
}

testing::AssertionResult equalWithGet(const vector& v, const Array& a) {
    if (a.size() != v.size())
        return testing::AssertionFailure()
             << "size mismatch " << v << " vs " << a;

    try {
        char x;
        for (size_t i = 0; i < a.size(); ++i) {
#if GRADUATE_STUDENT == 0
            if (a.get(x, i) == -1 || x != v[i])
                throw "mismatch";
#else
            a.get(x, i);
            if (x != v[i])
                throw "mismatch";
#endif
        }
    } catch (...) {
        return testing::AssertionFailure() << "value mismatch " << v << " vs " << a;
    }

    return testing::AssertionSuccess();
}

testing::AssertionResult equalWithSubscript(const vector& v, const Array& a) {
    if (a.size() != v.size())
        return testing::AssertionFailure()
            << "size mismatch " << v << " vs " << a;

    for (size_t i = 0; i < a.size(); ++i) {
        if (a[i] != v[i])
            return testing::AssertionFailure()
                << "value mismatch " << v << " vs " << a;
    }

    return testing::AssertionSuccess();
}

char* begin(Array& a) {
    return &a[0];
}

char* end(Array& a) {
    return begin(a) + a.size();
}

const char* cbegin(const Array& a) {
    return &a[0];
}

const char* cend(const Array& a) {
    return cbegin(a) + a.size();
}
