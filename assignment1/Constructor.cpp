#include "ArrayTestUtils.h"
#include <stdexcept>
#include <iterator>

const vector tenZeros(10, '0');
const vector tenOnes(10, '1');

TEST(Constructor, Size) {
    Array a(10), b(10), c(9);
    EXPECT_EQ(10u, a.size());
    EXPECT_EQ(10, std::distance(begin(a), end(a)));
    EXPECT_EQ(10, std::distance(cbegin(a), cend(a)));
}

TEST(Constructor, SizeAndValue) {
    Array a(10, '1'), b(10, '1'), c(10, '2'), d(9, '1');
    EXPECT_EQ(10u, a.size());
    EXPECT_EQ(10, std::distance(begin(a), end(a)));
    EXPECT_EQ(10, std::distance(cbegin(a), cend(a)));
    EXPECT_PRED2(equalWithIterators, tenOnes, a);
    EXPECT_PRED2(equalWithGet, tenOnes, a);
    EXPECT_PRED2(equalWithSubscript, tenOnes, a);
    EXPECT_EQ(a, b);
    EXPECT_NE(a, c);
    EXPECT_NE(a, d);
    EXPECT_NE(c, d);
}
