/***************************************
 Class: CS251
 Assignment Number: 1
 Honor Pledge: I pledge that I have not recieved nor given help on this assignment.
 ***************************************/

#ifndef ARRAY_C
#define ARRAY_C
#include <algorithm>

#include "Array.h"

#if !defined (__INLINE__)
#define INLINE
#include "Array.inl"
#endif /* __INLINE__ */

//creates uninitialized array
Array::Array(size_t size) :
		max_size_(size), cur_size_(size), array_(new T[cur_size_]) {
}

// Dynamically initialize an array.

Array::Array(size_t size, const T &default_value) :
		max_size_(size), cur_size_(size), array_(new T[cur_size_]) {
	for (size_t i = 0; i < cur_size_; ++i)
		array_[i] = default_value;
}

// The copy constructor (performs initialization).

Array::Array(const Array &s) :
		max_size_(s.max_size_), cur_size_(s.cur_size_), array_(new T[cur_size_]) {
	// @@ Please initialize your data members in the base/member initialization
	// section, as we discussed in the C++ Overview video (Part 2).

	// @@ Please use ++i instead of i++.
	// @@ size is a size_t, so please make i size_t instead of an int
	for (size_t i = 0; i < s.cur_size_; ++i)
		array_[i] = s[i];

}

// Compare this array with <s> for equality.

bool Array::operator==(const Array &s) const {
	// @@ Two arrays with different max sizes can have identical contents.
	// @@ You don't need to use max_size_ here:
	if (s.cur_size_ != cur_size_)
		return false;
	// @@ The else {} isn't necessary here - anything that matched
	// the previous condition has exited the function.
	// @@ Please use ++i instead of i++.
	for (size_t i = 0; i < cur_size_; ++i)
		if (array_[i] != s.array_[i])
			return false;

	return true;

}

// Compare this array with <s> for inequality.

bool Array::operator!=(const Array &s) const {
	return !(s == *this);
}

// Assignment operator (performs assignment).

Array &
Array::operator=(const Array &s) {
	if (this == &s)
		return *this;
	if (this->max_size_ > s.cur_size_) {
		for (size_t i = 0; i < s.cur_size_; ++i)
			array_[i] = s.array_[i];
		this->cur_size_ = s.cur_size_;
	} else {
		Array temp_(s);
		// @@ There's no need to delete this here; the temporary
		// will take care of that for you!
		delete[] array_;
		array_ = 0;
		std::swap(max_size_, temp_.max_size_);
		std::swap(cur_size_, temp_.cur_size_);
		std::swap(array_, temp_.array_);
	}
	return *this;
}

// Clean up the array (e.g., delete dynamically allocated memory).

Array::~Array(void) {
	delete[] this->array_;
	// @@ There's no need to zero out these members.
}

// = Set/get methods.

// Set an item in the array at location index.  Returns -1 if
// index is larger than the size() of the array, else 0.

int Array::set(const T &new_item, size_t index) {
	if (this->in_range(index)) {
		this->array_[index] = new_item;
		return 0;
	}
	return -1;
}

// Get an item in the array at location index.  Returns -1 if
// index is larger than the size() of the array, else 0.

int Array::get(T &item, size_t index) const {
	if (this->in_range(index)) {
		item = this->array_[index];
		return 0;
	}
	return -1;
}

#endif /* ARRAY_C */
