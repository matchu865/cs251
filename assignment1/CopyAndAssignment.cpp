#include "ArrayTestUtils.h"
#include <stdexcept>
#include <iterator>

const vector emptyVec;
const vector tenZeros(10, '0');
const vector tenOnes(10, '1');
const vector oneFiveVec{'1', '2', '3', '4', '5'};

TEST(CopyConstructor, Trivial) {
    Array a(10, '1');
    Array b(a);
    EXPECT_PRED2(equalWithIterators, tenOnes, b);
    EXPECT_PRED2(equalWithGet, tenOnes, b);
    EXPECT_PRED2(equalWithSubscript, tenOnes, b);
    EXPECT_EQ(a, b);

    EXPECT_NE(begin(a), begin(b));
    EXPECT_NE(cbegin(a), cbegin(b));
    EXPECT_NE(end(a), end(b));
    EXPECT_NE(cend(a), cend(b));
}

TEST(CopyConstructor, EmptyArrayConsistency) {
    Array empty(0);
    Array copy(empty);

    EXPECT_EQ(0u, copy.size());
    char shouldFail;
#if GRADUATE_STUDENT == 0
    EXPECT_EQ(-1, copy.get(shouldFail, 0));
#else
    EXPECT_THROW(copy.get(shouldFail, 0), std::out_of_range);
#endif

    EXPECT_PRED2(equalWithIterators, emptyVec, copy);
    EXPECT_PRED2(equalWithGet, emptyVec, copy);
    EXPECT_PRED2(equalWithSubscript, emptyVec, copy);

    EXPECT_EQ(begin(empty), end(empty));
    EXPECT_EQ(cbegin(empty), cend(empty));
    EXPECT_EQ(begin(copy), end(copy));
    EXPECT_EQ(cbegin(copy), cend(copy));

    if ((cbegin(empty) == nullptr && cbegin(copy) != nullptr)
            || (cbegin(empty) != nullptr && cbegin(copy) == nullptr))
        FAIL() << "Inconsistent empty Array behavior";
}

struct Assignment : testing::Test {
    Array oneFive;

    Assignment() : oneFive(5) {
        std::copy(oneFiveVec.cbegin(), oneFiveVec.cend(), begin(oneFive));
    }
};

TEST_F(Assignment, Simple) {
    Array a(0);
    a = a;
    EXPECT_PRED2(equalWithIterators, emptyVec, a);
    EXPECT_PRED2(equalWithGet, emptyVec, a);
    EXPECT_PRED2(equalWithSubscript, emptyVec, a);

    a = oneFive;
    EXPECT_EQ(a, oneFive);
    EXPECT_PRED2(equalWithIterators, oneFiveVec, a);
    EXPECT_PRED2(equalWithGet, oneFiveVec, a);
    EXPECT_PRED2(equalWithSubscript, oneFiveVec, a);
}

