#include "Queue.h"
#include "AQueue.h"
#include "LQueue.h"
#include "STLQueue.h"
#include "gtest/gtest.h"
#include <memory>

static const size_t DEFAULT_QUEUE_SIZE = 100;

struct Object {
  Object (void) : x_ (0) {}
  Object(int x) : x_ (x) {}
    bool operator==(const Object&s) const {return x_ == s.x_;}
  bool operator!=(const Object&s) const {return *this != s;}
    int x_;
};

typedef Queue<Object> QUEUE;
typedef Queue_Adapter<Object, LQueue<Object>> LQUEUE_ADAPTER;
typedef Queue_Adapter<Object, AQueue<Object>> AQUEUE_ADAPTER;
typedef Queue_Adapter<Object, STLQueue_Adapter<Object>> STLQUEUE_ADAPTER;
typedef std::unique_ptr<Queue<Object>> QPointer;

TEST(CompilationTest, AdapterArrayQueue) {


    AQUEUE_ADAPTER a(1);
    AQUEUE_ADAPTER b(10);
    AQUEUE_ADAPTER c(a);
    AQUEUE_ADAPTER d(DEFAULT_QUEUE_SIZE);
    a = b;
    d = b;
}

TEST(EnqueueDequeueTest, AdapterArrayQueue)  {

    AQUEUE_ADAPTER q1(1);
    AQUEUE_ADAPTER q2(DEFAULT_QUEUE_SIZE);
    EXPECT_EQ (q1, q2);
    EXPECT_EQ (q1.is_empty (), true);
    EXPECT_EQ (q1.is_full (), false);
    EXPECT_EQ (q1.size (), 0);
    EXPECT_NO_THROW (q1.enqueue(Object(0)));
    EXPECT_EQ (Object(0), q1.front ());
    EXPECT_EQ (q1.is_full (), true);
    EXPECT_EQ (q1.is_empty (), false);
    EXPECT_EQ (q1.size (), 1);
    EXPECT_NO_THROW (q2.enqueue(Object(1)));
    EXPECT_EQ (Object(1), q2.front ());
    EXPECT_EQ (q2.is_full (), false);
    EXPECT_NE (q2.is_empty (), true);
    EXPECT_NE (q1, q2);
    q1.dequeue();
    q2.dequeue();

    EXPECT_THROW(q1.front(), QUEUE::Underflow);
    EXPECT_THROW(q2.front(), QUEUE::Underflow);
}

TEST(EqualityNonEqualityTest, AdapterArrayQueue) {


    AQUEUE_ADAPTER q(1), p(DEFAULT_QUEUE_SIZE);
    EXPECT_NE (q.is_empty(), q.is_full());
    EXPECT_EQ (q.size(), p.size());
    EXPECT_NE ((q == p), (p != q));
}

TEST(CompilationTest, AdapterLinkedQueue) {

    LQUEUE_ADAPTER a(1);
    LQUEUE_ADAPTER b(10);
    LQUEUE_ADAPTER c(a);
    LQUEUE_ADAPTER d(DEFAULT_QUEUE_SIZE);
    a = b;
    d = b;
}

TEST(EnqueueDequeueTest, AdapterLinkedQueue) {
    LQUEUE_ADAPTER q1(1);
    LQUEUE_ADAPTER q2(DEFAULT_QUEUE_SIZE);
    EXPECT_EQ (q1, q2);
    EXPECT_EQ (q1.is_empty (), true);
    EXPECT_EQ (q1.is_full (), false);
    EXPECT_EQ (q1.size (), 0);
        EXPECT_NO_THROW (q1.enqueue(Object(0)));
    EXPECT_EQ (Object(0), q1.front ());
    EXPECT_EQ (q1.is_full (), false);
    EXPECT_EQ (q1.is_empty (), false);
    EXPECT_EQ (q1.size (), 1);
    EXPECT_NO_THROW (q2.enqueue(Object(1)));
    EXPECT_EQ (Object(1), q2.front ());
    EXPECT_EQ (q2.is_full (), false);
    EXPECT_EQ (q2.is_empty (), false);
    EXPECT_NE (q1, q2);
    q1.dequeue();
    q2.dequeue();

    EXPECT_THROW(q1.front(), QUEUE::Underflow);
    EXPECT_THROW(q2.front(), QUEUE::Underflow);
}

TEST(EqualityNonEqualityTest, AdapterLinkedQueue) {
    LQUEUE_ADAPTER q(1), p(DEFAULT_QUEUE_SIZE);
    EXPECT_NE (q.is_empty(), q.is_full());
    EXPECT_EQ (q.size(), p.size());
    EXPECT_NE ((q == p), (p != q));
}

TEST(CompilationTest, AdapterSTLQueue) {

    STLQUEUE_ADAPTER a(1);
    STLQUEUE_ADAPTER b(10);
    STLQUEUE_ADAPTER c(a);
    STLQUEUE_ADAPTER d(DEFAULT_QUEUE_SIZE);
    a = b;
    d = b;
}

TEST(EnqueueDequeueTest, AdapterSTLQueue) {

    STLQUEUE_ADAPTER q1(1);
    STLQUEUE_ADAPTER q2(DEFAULT_QUEUE_SIZE);
    //EXPECT_EQ (q1, q2);
    EXPECT_EQ (q1.is_empty (), true);
    EXPECT_EQ (q1.is_full (), false);
    EXPECT_EQ (q1.size (), 0);
    EXPECT_NO_THROW (q1.enqueue(Object(0)));
    EXPECT_EQ (Object(0), q1.front ());
    EXPECT_EQ (q1.is_full (), false);
    EXPECT_EQ (q1.is_empty (), false);
    EXPECT_EQ (q1.size (), 1);
    EXPECT_NO_THROW (q2.enqueue(Object(1)));
    EXPECT_EQ (Object(1), q2.front ());
    EXPECT_EQ (q2.is_full (), false);
    EXPECT_EQ (q2.is_empty (), false);
    //EXPECT_NE (q1, q2);
    EXPECT_NO_THROW (q1.dequeue());
    EXPECT_NO_THROW (q2.dequeue());

    EXPECT_THROW(q1.front(), QUEUE::Underflow);
    EXPECT_THROW(q2.front(), QUEUE::Underflow);
}

TEST(EqualityNonEqualityTest, AdapterSTLQueue) {

    STLQUEUE_ADAPTER q(1), p(DEFAULT_QUEUE_SIZE);
    EXPECT_NE (q.is_empty(), q.is_full());
    EXPECT_EQ (q.size(), p.size());
    //EXPECT_NE ((q == p), (p != q));
}

TEST(CompilationTest, Queue) {

    QPointer a(new LQUEUE_ADAPTER(1));
    QPointer b(new LQUEUE_ADAPTER(DEFAULT_QUEUE_SIZE));
    QPointer c(new AQUEUE_ADAPTER(1));
    QPointer d(new AQUEUE_ADAPTER(DEFAULT_QUEUE_SIZE));
    QPointer e(new STLQUEUE_ADAPTER(1));
    QPointer f(new STLQUEUE_ADAPTER(DEFAULT_QUEUE_SIZE));
}

TEST(AQueue_Adapter, Queue) {

    QPointer q(new AQUEUE_ADAPTER(1)), p(new AQUEUE_ADAPTER(10));
    EXPECT_NE (q->is_empty(), q->is_full());
    EXPECT_EQ (q->size(), 0);
    EXPECT_THROW(q->front(), QUEUE::Underflow);
    EXPECT_THROW(q->dequeue(), QUEUE::Underflow);
    q->enqueue(Object(1));
    EXPECT_NE (q->is_empty(), q->is_full());
    EXPECT_EQ (q->size(), 1);
//Try to throw an Overflow Exception
    EXPECT_THROW(q->enqueue(Object(1)), QUEUE::Overflow);
   // EXPECT_THROW(q->enqueue(Object()), AQUEUE_ADAPTER::Overflow);
}

TEST(LQueue_Adapter, Queue) {
    QPointer q(new LQUEUE_ADAPTER(1));
    EXPECT_NE (q->is_empty(), q->is_full());
    EXPECT_EQ (q->size(), 0);
    q->enqueue(Object(1));
    EXPECT_EQ (q->size (), 1);
    q->dequeue();
    EXPECT_THROW(q->front(), QUEUE::Underflow);
    EXPECT_THROW(q->dequeue(), QUEUE::Underflow);
}

TEST(STLQueue_Adapter, Queue) {
    QPointer q(new STLQUEUE_ADAPTER(1)), p(new STLQUEUE_ADAPTER(10));
    EXPECT_EQ (q->is_empty(), true);
    EXPECT_EQ (q->is_full(), false);
    EXPECT_EQ (q->size(), 0);
    EXPECT_THROW(q->front(), QUEUE::Underflow);
    EXPECT_THROW(q->dequeue(), QUEUE::Underflow);
    q->enqueue(Object(1));
}

TEST(Clone, Queue_Adapter) {

    AQUEUE_ADAPTER a(10);
    LQUEUE_ADAPTER b(10);
    STLQUEUE_ADAPTER c(10);

    std::unique_ptr<AQUEUE_ADAPTER> cloneA (a.clone());
    EXPECT_NE (&a, cloneA.get ());
    std::unique_ptr<LQUEUE_ADAPTER> cloneB (b.clone());
    EXPECT_NE (&b, cloneB.get ());
    std::unique_ptr<STLQUEUE_ADAPTER> cloneC (c.clone());
    EXPECT_NE (&c, cloneC.get ());
}
