/* -*- C++ -*- */

/***************************************
Class: CS251
Assignment Number: 4b
Honor Pledge: I pledge that I have not recieved nor given help on this assignment.
***************************************/

#ifndef _Factories_H
#define _Factories_H

#include "Tree.h"
#include "Traversal_Strategy_Impl.h"

// Method to create the binary tree to traverse.
extern TREE make_tree (void);

// Method to create a tree traversal strategy. Delegate it to the
// implementation.
extern Traversal_Strategy_Impl *make_traversal_strategy (const std::string &);

#endif /* _Factories_H */
