/* -*- C++ -*- */

/***************************************
Class: CS251
Assignment Number: 4b
Honor Pledge: I pledge that I have not recieved nor given help on this assignment.
***************************************/

#if !defined (_Factories_CPP)
#define _Factories_CPP

#include <string>

#include "Factories.h"
#include "Traversal_Strategy_Impl.h"
#include "Options.h"

// Method to create the binary tree to traverse.
TREE
make_tree (void)
{
  //             +
  //        /         \
  //      -             *
  //    /   \         /   \
  //  3       4     7       5
  // Make/return a balanced expression tree that matches what's shown above.
	  NODE *t1 = new NODE ('3');
	  NODE *t2 = new NODE ('4');
	  NODE *t3 = new NODE ('-', t1, t2);
	  NODE *t4 = new NODE ('7');
	  NODE *t5 = new NODE ('5');
	  NODE *t6 = new NODE ('*', t4, t5);
	  return TREE ('+', t3, t6);
}

// Pass method call to implementation for strategy creation.
Traversal_Strategy_Impl *
make_traversal_strategy (const std::string &name)
{
  if(name == "Levelorder")
	  return new Level_Order_Traversal_Strategy;
  if(name == "Preorder")
	  return new Pre_Order_Traversal_Strategy;
  if(name == "Postorder")
	  return new Post_Order_Traversal_Strategy;
  if(name == "Inorder")
	  return new In_Order_Traversal_Strategy;
  throw Traversal_Strategy_Impl::Unknown_Strategy(name);
}

#endif /* _Factories_CPP */
