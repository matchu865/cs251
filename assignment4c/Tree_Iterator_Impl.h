/***************************************
Class: CS251
Assignment Number: 4c
Honor Pledge: I pledge that I have not recieved nor given help on this assignment.
***************************************/

#ifndef _TREE_ITERATOR_H
#define _TREE_ITERATOR_H
#include <memory>

#include "Queue.h"
#include "Typedefs.h"

// Forward declaration
class Visitor;

/// Unknown_Strategy class for exceptions when an unknown strategy
/// name.

class Unknown_Strategy
{
public:
  Unknown_Strategy(const std::string &msg): msg_ (msg) {}

  const std::string what(void) { return msg_; }

private:
  std::string msg_;
};

class Tree_Iterator_Impl
{
public:
  // Destructor.
  virtual ~Tree_Iterator_Impl(void);

  /// Dereference operator returns a reference to the item contained
  /// at the current position.
  virtual TREE current_item (void) = 0;

  /// Increment the iterator.
  virtual void advance (void) = 0;

  /// Check whether the iterator is done.
  virtual bool done (void) = 0;
};

class Level_Order_Tree_Iterator_Impl : public Tree_Iterator_Impl
{
public:
  // Constructor.
  Level_Order_Tree_Iterator_Impl(const TREE & tree);

  // Destructor.
  virtual ~Level_Order_Tree_Iterator_Impl(void);

  /// Dereference operator returns a reference to the item contained
  /// at the current position.
  virtual TREE current_item (void);

  /// Increment the iterator.
  virtual void advance (void);

  /// Check whether the iterator is done.
  virtual bool done (void);
   
private:
  /// Factory method to create the appropriate type of queue.
  void make_queue_strategy (const std::string &queue_type);

  std::unique_ptr<QUEUE> myqueue_;
};

#endif /* _TREE_ITERATOR_H */
