/* -*- C++ -*- */

/***************************************
Class: CS251
Assignment Number: 4b
Honor Pledge: I pledge that I have not recieved nor given help on this assignment.
***************************************/

#if !defined (_Traversal_Strategy_Impl_CPP)
#define _Traversal_Strategy_Impl_CPP

#include <iostream>

#include "Traversal_Strategy_Impl.h"
#include "Options.h"
#include "Node.h"
#include "Queue.h"
#include "Typedefs.h"

// Size for the AQueue.
const size_t AQUEUE_SIZE = 80;

// This needs to be virtual so that subclasses are correctly destroyed.
Traversal_Strategy_Impl::~Traversal_Strategy_Impl ()
{
}

// Ctor
Level_Order_Traversal_Strategy::Level_Order_Traversal_Strategy ()
{
	make_queue_strategy(Options::instance()->queue_type());
}

// Dtor
Level_Order_Traversal_Strategy::~Level_Order_Traversal_Strategy ()
{
	//no op
}

// Traverse the tree in level order.
void
Level_Order_Traversal_Strategy::traverse (const TREE &root)
{
  if(!(root.is_null()))
	queue_->enqueue(root);
  while(!(queue_->is_empty())){
	  TREE tree_ = queue_->front();
	  std::cout << tree_.item();
	  if(!(tree_.left().is_null()))
			  queue_->enqueue(tree_.left());
	  if(!(tree_.right().is_null()))
			  queue_->enqueue(tree_.right());
	  queue_->dequeue();
  }
}

// A Factory method for creating queues.
void
Level_Order_Traversal_Strategy::make_queue_strategy (const std::string &queue_type)
{
     if(queue_type == "AQueue"){
	   queue_ = std::auto_ptr<QUEUE>(new AQUEUE_ADAPTER(AQUEUE_SIZE));
	   return;
     }
     if(queue_type == "LQueue"){
	   queue_ = std::auto_ptr<QUEUE>(new LQUEUE_ADAPTER(0));
       return;
     }
     throw Traversal_Strategy_Impl::Unknown_Strategy(queue_type);
}

// Traverse the tree in preorder.
void
Pre_Order_Traversal_Strategy::traverse (const TREE &root)
{
	if(root.is_null()) return;
	std::cout << root.item();
	traverse(root.left());
	traverse(root.right());
}

// Traverse the tree in postorder.
void
Post_Order_Traversal_Strategy::traverse (const TREE &root)
{
	if(root.is_null()) return;
	traverse(root.left());
	traverse(root.right());
	std::cout << root.item();
}

// Traverse the tree in inorder.
void
In_Order_Traversal_Strategy::traverse (const TREE &root)
{
	if(root.is_null()) return;
	traverse(root.left());
	std::cout << root.item();
	traverse(root.right());
}

#endif /* _Traversal_Strategy_Impl_CPP */
