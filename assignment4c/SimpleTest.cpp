// Uncomment the following line, changing it to 1 if you are a graduate student.
#define GRADUATE_STUDENT 0
#ifndef GRADUATE_STUDENT
//#error You need to look at the first line of SimpleTest.cpp
#endif

#include "gtest/gtest.h"

#include <iostream>
#include <memory>
#include <string>
#include <cstdio>
#include <string>

// Yeah, this is dirty, but the only way to test certain error cases.
#define private public
#include "Refcounter.h"
#include "Options.h"

# if GRADUATE_STUDENT == 1
#  include "Traversal_Strategy.h"
# endif

#undef private

#include "Traversal_Strategy_Impl.h"
#include "Tree.h"

#if GRADUATE_STUDENT ==1
# include "Tree_Factory.h"
#else
# include "Factories.h"
#endif

#include "getopt.h"

#if GRADUATE_STUDENT == 1
#define MAKE_TREE Tree_Factory ().make_tree ()
Traversal_Strategy MAKE_TRAVERSAL_STRATEGY(const std::string &str)
{
  std::string tmp (Options::instance ()->traversal_strategy_);
  Options::instance ()->traversal_strategy_ = str;
  Traversal_Strategy t = Tree_Factory ().make_traversal_strategy ();
  Options::instance ()->traversal_strategy_ = tmp;
  return t;
}

class TRAVERSAL_HOLDER : private Traversal_Strategy
{
public:
  TRAVERSAL_HOLDER (void) : Traversal_Strategy (new Null_Traversal_Strategy ()) {}
  TRAVERSAL_HOLDER (const Traversal_Strategy &s) : Traversal_Strategy (s) {}
  TRAVERSAL_HOLDER (Traversal_Strategy_Impl *s) : Traversal_Strategy (s) {}
  void reset (const Traversal_Strategy &s)
  {
    Traversal_Strategy::operator=(s);
  }

  Traversal_Strategy_Impl * get (void)
  {
    return Traversal_Strategy::strategy_impl_.ptr_;
  }
};

#else
#define MAKE_TREE make_tree ()
#define MAKE_TRAVERSAL_STRATEGY(X) make_traversal_strategy (X)
typedef std::unique_ptr <Traversal_Strategy_Impl> TRAVERSAL_HOLDER;
#endif

#define RESET_OPTIONS()                         \
  do {    \
delete Options::options_impl_;                  \
Options::options_impl_ = 0;                     \
} while (0)

int argc = 5;

TREE make_leaning_tree () {
  //             +
  //        /         |
  //      *             /
  //    /   |         /   |
  //  1       3     *       9
  //               / |
  //              5   7

//Pre-Order    +*13/*579
//In-Order     1*3+5*7/9
//Post-Order   13*57*9/+
//Level-Order  +*/13*957


  NODE *t1 = new NODE ('1');
  NODE *t2 = new NODE ('3');
  NODE *t3 = new NODE ('*', t1, t2);
  NODE *t4 = new NODE ('5');
  NODE *t5 = new NODE ('7');
  NODE *t6 = new NODE ('*', t4, t5);
  NODE *t7 = new NODE ('9');
  NODE *t8 = new NODE ('/', t6, t7);
  TREE tree = TREE ('+', t3, t8);

  return tree;
}

void cleanup_options (void)
{
  RESET_OPTIONS();
}

bool traverseTest(TREE *tree, Traversal_Strategy_Impl *strategy, std::string str) {
    //Create buffer in which to redirect std::cout to
    std::stringstream buffer;

    // Save cout's buffer here
    std::streambuf *sbuf = std::cout.rdbuf();

    std::cout.flush ();
    // Redirect cout to the stringstream buffer
    std::cout.rdbuf(buffer.rdbuf());


    EXPECT_NO_THROW (strategy->traverse (*tree));
    std::cout.flush ();
    // Redirect cout back to its old self
    std::cout.rdbuf(sbuf);


    char c;
    //std::cout << "\nGot traversal: ";
    for(std::string::iterator i = str.begin(); i != str.end(); i++ ) {
        c = buffer.get();

        if (c == EOF)
          {
            std::cout << std::endl << "Reached the end of input.\n";
            return false;
          }
        //std::cout << c;
        if(*i != c) {
          std::cout<<std::endl<<"\nIn the Tree String "<<str;

          std::cout<<std::endl<<"Expected "<< *i;
          std::cout<<std::endl<<"Actual "<< c << std::endl;
            return false;
        }
    }

 return true;
}

TEST(Singleton, OptionsInstance) {
   atexit (cleanup_options);
   Options *o1 = Options::instance();

   {
       Options *o2 = Options::instance();
       EXPECT_EQ(o1, o2);
   }

   Options *o3 = Options::instance();
   EXPECT_EQ(o1, o3);
}


TEST(MakeSimpleTree, DefaultQueueType) {
    TREE tree = MAKE_TREE;
}

TEST(MakeLeaningTree, DefaultQueueType) {
    TREE tree = make_leaning_tree();
}

TEST(TraverseSimpleTree, DefaultQueueType) {
  TRAVERSAL_HOLDER  pre_order (MAKE_TRAVERSAL_STRATEGY ("Preorder"));
  TRAVERSAL_HOLDER  in_order (MAKE_TRAVERSAL_STRATEGY ("Inorder"));
  TRAVERSAL_HOLDER  post_order (MAKE_TRAVERSAL_STRATEGY ("Postorder"));
  TRAVERSAL_HOLDER  level_order (MAKE_TRAVERSAL_STRATEGY ("Levelorder"));

  TREE tree = MAKE_TREE;

#if GRADUATE_STUDENT == 0
  EXPECT_PRED3(traverseTest, &tree, pre_order.get (), "+-34*75");
  EXPECT_PRED3(traverseTest, &tree, in_order.get (), "3-4+7*5");
  EXPECT_PRED3(traverseTest, &tree, post_order.get (), "34-75*+");
  EXPECT_PRED3(traverseTest, &tree, level_order.get (), "+-*3475");
#else
  EXPECT_PRED3(traverseTest, &tree, pre_order.get (), "*+15-79");
  EXPECT_PRED3(traverseTest, &tree, in_order.get (), "1+5*7-9");
  EXPECT_PRED3(traverseTest, &tree, post_order.get (), "15+79-*");
  EXPECT_PRED3(traverseTest, &tree, level_order.get (), "*+-1579");
#endif
}

TEST(TraverseLeaningTree, DefaultQueueType) {
  TRAVERSAL_HOLDER  pre_order (MAKE_TRAVERSAL_STRATEGY ("Preorder"));
  TRAVERSAL_HOLDER  in_order (MAKE_TRAVERSAL_STRATEGY ("Inorder"));
  TRAVERSAL_HOLDER  post_order (MAKE_TRAVERSAL_STRATEGY ("Postorder"));
  TRAVERSAL_HOLDER  level_order (MAKE_TRAVERSAL_STRATEGY ("Levelorder"));

  TREE tree = make_leaning_tree();

  EXPECT_PRED3(traverseTest, &tree, pre_order.get (), "+*13/*579");
  EXPECT_PRED3(traverseTest, &tree, in_order.get (), "1*3+5*7/9");
  EXPECT_PRED3(traverseTest, &tree, post_order.get (), "13*57*9/+");
  EXPECT_PRED3(traverseTest, &tree, level_order.get (), "+*/13*957");
}


TEST(PreOrderSimpleTree, AQueue) {
    parsing::optind = 0;
    const char *argv[] = { "Adapter_test", "-t", "P", "-q", "A"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Preorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("AQueue", Options::instance ()->queue_type ());

    TRAVERSAL_HOLDER traversal_strategy;
    EXPECT_NO_THROW (traversal_strategy.reset (
      MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));
    TREE tree = MAKE_TREE;

#if GRADUATE_STUDENT == 0
    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "+-34*75");
#else
    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "*+15-79");
#endif
}



TEST(InOrderSimpleTree, AQueue) {

    parsing::optind = 0;
    const char *argv[] = { "Adapter_test", "-t", "I", "-q", "A"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Inorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("AQueue", Options::instance ()->queue_type ());

    TRAVERSAL_HOLDER traversal_strategy (MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ()));
    TREE tree = MAKE_TREE;

#if GRADUATE_STUDENT == 0
    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "3-4+7*5");
#else
    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "1+5*7-9");
#endif
}

TEST(PostOrderSimpleTree, AQueue) {

    parsing::optind = 0;
    const char *argv[] = { "Adapter_test", "-t", "O", "-q", "A"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Postorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("AQueue", Options::instance ()->queue_type ());

        TRAVERSAL_HOLDER traversal_strategy;
    EXPECT_NO_THROW (traversal_strategy.reset (
      MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));
    TREE tree = MAKE_TREE;

#if GRADUATE_STUDENT == 0
    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "34-75*+");
#else
    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "15+79-*");
#endif
}

TEST(LevelOrderSimpleTree, AQueue) {

    parsing::optind = 0;
    const char *argv[] = { "Adapter_test", "-t", "L", "-q", "A"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Levelorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("AQueue", Options::instance ()->queue_type ());

    TRAVERSAL_HOLDER traversal_strategy (MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ()));
    TREE tree = MAKE_TREE;

#if GRADUATE_STUDENT == 0
    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "+-*3475");
#else
    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "*+-1579");
#endif
}

TEST(LevelOrderSimpleTree, LQueue) {

    parsing::optind = 0;
    const char *argv[] = { "Adapter_test", "-t", "L", "-q", "L"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Levelorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("LQueue", Options::instance ()->queue_type ());

        TRAVERSAL_HOLDER traversal_strategy;
    EXPECT_NO_THROW (traversal_strategy.reset (
      MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));
    TREE tree = MAKE_TREE;

#if GRADUATE_STUDENT == 0
    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "+-*3475");
#else
    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "*+-1579");
#endif
}

TEST(PreOrderLeaningTree, AQueue) {
    parsing::optind = 0;
    const char *argv[] = { "Adapter_test", "-t", "P", "-q", "A"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Preorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("AQueue", Options::instance ()->queue_type ());

        TRAVERSAL_HOLDER traversal_strategy;
    EXPECT_NO_THROW (traversal_strategy.reset (
      MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));
    TREE tree = make_leaning_tree();

    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "+*13/*579");
}



TEST(InOrderLeaningTree, AQueue) {

    parsing::optind = 0;
    const char *argv[] = { "Adapter_test", "-t", "I", "-q", "A"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Inorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("AQueue", Options::instance ()->queue_type ());

        TRAVERSAL_HOLDER traversal_strategy;
    EXPECT_NO_THROW (traversal_strategy.reset (
      MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));
    TREE tree = make_leaning_tree();

    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "1*3+5*7/9");
}

TEST(PostOrderLeaningTree, AQueue) {

    parsing::optind = 0;
    const char *argv[] = { "Adapter_test", "-t", "O", "-q", "A"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Postorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("AQueue", Options::instance ()->queue_type ());

        TRAVERSAL_HOLDER traversal_strategy;
    EXPECT_NO_THROW (traversal_strategy.reset (
      MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));
    TREE tree = make_leaning_tree();

    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "13*57*9/+");
}

TEST(LevelOrderLeaningTree, AQueue) {

    parsing::optind = 0;
    const char *argv[] = { "Adapter_test", "-t", "L", "-q", "A"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Levelorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("AQueue", Options::instance ()->queue_type ());

        TRAVERSAL_HOLDER traversal_strategy;
    EXPECT_NO_THROW (traversal_strategy.reset (
      MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));
    TREE tree = make_leaning_tree();

    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "+*/13*957");
}

TEST(PreOrderLeaningTree, LQueue) {
    parsing::optind = 0;
    const char *argv[] = { "Adapter_test", "-t", "P", "-q", "L"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Preorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("LQueue", Options::instance ()->queue_type ());

        TRAVERSAL_HOLDER traversal_strategy;
    EXPECT_NO_THROW (traversal_strategy.reset (
      MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));
    TREE tree = make_leaning_tree();

    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "+*13/*579");
}



TEST(InOrderLeaningTree, LQueue) {

    parsing::optind = 0;
    const char *argv[] = { "Adapter_test", "-t", "I", "-q", "L"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Inorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("LQueue", Options::instance ()->queue_type ());

        TRAVERSAL_HOLDER traversal_strategy;
    EXPECT_NO_THROW (traversal_strategy.reset (
      MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));
    TREE tree = make_leaning_tree();

    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "1*3+5*7/9");
}

TEST(PostOrderLeaningTree, LQueue) {

    parsing::optind = 0;
    const char *argv[] = { "Adapter_test", "-t", "O", "-q", "L"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Postorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("LQueue", Options::instance ()->queue_type ());

        TRAVERSAL_HOLDER traversal_strategy;
    EXPECT_NO_THROW (traversal_strategy.reset (
      MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));
    TREE tree = make_leaning_tree();

    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "13*57*9/+");
}

TEST(LevelOrderLeaningTree, LQueue) {

    parsing::optind = 0;
    const char *argv[] = { "Adapter_test", "-t", "L", "-q", "L"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Levelorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("LQueue", Options::instance ()->queue_type ());

    TRAVERSAL_HOLDER traversal_strategy;
    EXPECT_NO_THROW (traversal_strategy.reset (
      MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));
    TREE tree = make_leaning_tree();

    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "+*/13*957");
}

 TEST(BadStrategy, TraversalType)
   {
     EXPECT_THROW (MAKE_TRAVERSAL_STRATEGY ("Asdf"),
                   Traversal_Strategy_Impl::Unknown_Strategy);
   }

 TEST(BadStrategy, QueueType)
   {
     Options *opts = Options::instance ();
     opts->queue_type_ = "asdf";
     EXPECT_THROW (MAKE_TRAVERSAL_STRATEGY ("Levelorder"),
                   Traversal_Strategy_Impl::Unknown_Strategy);
   }

#if GRADUATE_STUDENT == 1
//This only tests the Level_Order_Traversal_strategy
TEST (GRAD, FACTORY) {
/*    parsing::optind = 0;
    const char *argv[] = { "Adapter_test", "-t", "L", "-q", "L"};

    Options::instance ()->parse_args (argc, (char **)argv);


    // Create a factory.
    Tree_Factory factory;

    // Use the factory to create the tree.
    TREE tree = factory.make_tree ();

    Binary_Tree_Factory_Impl binaryFactory;
    TREE binaryTree = binaryFactory.make_tree ();
     // Use the factory to create desired traversal strategy.
    Traversal_Strategy traversal_strategy = factory.MAKE_TRAVERSAL_STRATEGY ();

    Traversal_Strategy binary_traversal_strategy = binaryFactory.MAKE_TRAVERSAL_STRATEGY ();
  */    // Use the specified traversal strategy to traverse the tree.
//    EXPECT_PRED3(traverseTest, &tree, traversal_strategy.get (), "+*/13*957");

//    EXPECT_PRED3(traverseTest, &binaryTree, binary_traversal_strategy, "+*/13*957");
}
#endif
