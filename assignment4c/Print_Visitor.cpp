/* -*- C++ -*- */

/***************************************
Class: CS251
Assignment Number: 4c
Honor Pledge: I pledge that I have not recieved nor given help on this assignment.
***************************************/

#if !defined (_PRINT_VISITOR_CPP)
#define _PRINT_VISITOR_CPP

#include <iostream>
#include <memory>
#include "Print_Visitor.h"
#include "Tree.h"

void
Print_Visitor::visit (NODE &tree)
{
  std::cout << tree.item();
}

#endif /* _PRINT_VISITOR_CPP */
