/* -*- C++ -*- */

/***************************************
Class: CS251
Assignment Number: 4b
Honor Pledge: I pledge that I have not recieved nor given help on this assignment.
***************************************/

#ifndef _Factories_H
#define _Factories_H

#include "Tree.h"

// Method to create the binary tree to traverse.
extern TREE make_tree (void);

#endif /* _Factories_H */
