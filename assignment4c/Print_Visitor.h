/* -*- C++ -*- */

/***************************************
Class: CS251
Assignment Number: 4c
Honor Pledge: I pledge that I have not recieved nor given help on this assignment.
***************************************/

#ifndef _PRINT_VISITOR_H
#define _PRINT_VISITOR_H

#include "Visitor.h"

/**
 * @class Visitor
 * @brief Defines a subclass for visiting TREE nodes and printing their contents.
 */

class Print_Visitor : public Visitor
{
public:
    // Visit the NODE.
    virtual void visit (NODE &tree);
};

#endif /* _PRINT_VISITOR_H */
