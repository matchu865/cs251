// Uncomment the following line, changing it to 1 if you are a graduate student.
 #define GRADUATE_STUDENT 0
#ifndef GRADUATE_STUDENT
#error You need to look at the first line of SimpleTest.cpp
#endif

#include "gtest/gtest.h"

#include <iostream>
#include <memory>
#include <string>
#include <cstdio>
#include <string>


// Yeah, this is dirty, but the only way to test certain error cases.
#define private public
#include "Refcounter.h"
#include "Options.h"

//# if GRADUATE_STUDENT == 1
//#  include "Traversal_Strategy.h"
//# endif

#undef private


#include "Tree.h"
#if GRADUATE_STUDENT ==1
# include "Tree_Factory.h"
#else
# include "Factories.h"
#endif

#include "getopt.h"
#include "Options.h"

# include "Print_Visitor.h"

#if GRADUATE_STUDENT == 1
#define MAKE_TREE Tree_Factory ().make_tree ()

struct acceptor
{
    acceptor (Visitor &v): visitor_(v) {}
    void operator () (TREE t) { t.accept (visitor_);}
    Visitor &visitor_;
};


TREE::iterator MAKE_TRAVERSAL_STRATEGY(const std::string &str)
{
  std::string tmp (Options::instance ()->traversal_strategy_);
  Options::instance ()->traversal_strategy_ = str;
  TREE tree = MAKE_TREE;
  TREE::iterator t = tree.begin (Options::instance ()->traversal_strategy ());
  Options::instance ()->traversal_strategy_ = tmp;
  return t;
}


/*
class TRAVERSAL_HOLDER : private Traversal_Strategy
{
public:
  TRAVERSAL_HOLDER (void) : Traversal_Strategy (new Null_Traversal_Strategy ()) {}
  TRAVERSAL_HOLDER (const Traversal_Strategy &s) : Traversal_Strategy (s) {}
  TRAVERSAL_HOLDER (Traversal_Strategy_Impl *s) : Traversal_Strategy (s) {}
  void reset (const Traversal_Strategy &s)
  {
    Traversal_Strategy::operator=(s);
  }

  Traversal_Strategy_Impl * get (void)
  {
    return Traversal_Strategy::strategy_impl_.ptr_;
  }
};
*/
#else
#define MAKE_TREE make_tree ()

TREE::iterator * MAKE_TRAVERSAL_STRATEGY(const std::string &str)
{
  std::string tmp (Options::instance ()->traversal_strategy_);
  Options::instance ()->traversal_strategy_ = str;
  TREE tree = MAKE_TREE;
  TREE::iterator *itr = tree.create_iterator (Options::instance ()->traversal_strategy ());
  Options::instance ()->traversal_strategy_ = tmp;
  return itr;
}

/*
#define MAKE_TRAVERSAL_STRATEGY(X) make_traversal_strategy (X)
typedef std::unique_ptr <Traversal_Strategy_Impl> TRAVERSAL_HOLDER;
*/

#endif

#define RESET_OPTIONS()                         \
  do {    \
delete Options::options_impl_;                  \
Options::options_impl_ = 0;                     \
} while (0)

int argc = 5;

TREE make_leaning_tree () {
  //             +
  //        /         |
  //      *             /
  //    /   |         /   |
  //  1       3     *       9
  //               / |
  //              5   7

//Pre-Order    +*13/*579
//In-Order     1*3+5*7/9
//Post-Order   13*57*9/+
//Level-Order  +*/13*957


  NODE *t1 = new NODE ('1');
  NODE *t2 = new NODE ('3');
  NODE *t3 = new NODE ('*', t1, t2);
  NODE *t4 = new NODE ('5');
  NODE *t5 = new NODE ('7');
  NODE *t6 = new NODE ('*', t4, t5);
  NODE *t7 = new NODE ('9');
  NODE *t8 = new NODE ('/', t6, t7);
  TREE tree = TREE ('+', t3, t8);

  return tree;
}

void cleanup_options (void)
{
  RESET_OPTIONS();
}

class Appender : public virtual Visitor
{
public:
  Appender (void) {}
  virtual ~Appender (void) {}
  virtual void visit (const NODE &node)
  {
    str_ += node.item ();
  }
  std::string result (void) { return str_; }
private:
  std::string str_;
};

#if GRADUATE_STUDENT == 1
bool traverseTest(TREE *tree, std::string str) {
    //Create buffer in which to redirect std::cout to
    std::stringstream buffer;

    // Save cout's buffer here
    std::streambuf *sbuf = std::cout.rdbuf();

    std::cout.flush ();
    // Redirect cout to the stringstream buffer
    std::cout.rdbuf(buffer.rdbuf());

    Print_Visitor visitor;

    Appender app;

    // Use the STL for_each() algorithm.
    std::for_each ((*tree).begin (Options::instance ()->traversal_strategy ()),
                   (*tree).end (Options::instance ()->traversal_strategy ()),
                   acceptor (visitor));

    std::for_each ((*tree).begin (Options::instance ()->traversal_strategy ()),
                   (*tree).end (Options::instance ()->traversal_strategy ()),
                   acceptor (app));


    // Use an explicit for loop.
    for (TREE::iterator iter = (*tree).begin (Options::instance ()->traversal_strategy ());
           iter != (*tree).end (Options::instance ()->traversal_strategy ());
           ++iter)
        (*iter).accept (visitor);

    EXPECT_EQ (app.result (), str);

//    EXPECT_NO_THROW (strategy->traverse (*tree));
    std::cout.flush ();
    // Redirect cout back to its old self
    std::cout.rdbuf(sbuf);

    //Doubling the Reference String since we are Iterating twice through the Tree
    str = str+str;

    //    std::cerr << "Full str: " << buffer.str () << std::endl;

    char c;
    //    std::cout << "\nGot traversal: ";
    for(std::string::iterator i = str.begin(); i != str.end(); i++ ) {
        c = buffer.get();

        if (c == EOF)
          {
            std::cout << std::endl << "Reached the end of input.\n";
            return false;
          }
        //        std::cout << c;
        if(*i != c) {
          std::cout<<std::endl<<"Failed Traversal For One Node.";
          std::cout<<std::endl<<"Make sure you are not printing spaces between the items in each Node.";
          std::cout<<std::endl<<"\nIn the Tree String "<<str;

          std::cout<<std::endl<<"Expected "<< *i;
          std::cout<<std::endl<<"Actual "<< c << std::endl;

         // Comment this so that students can see the whole string and debug it
            return false;
        }
    }
 return true;
}

#else

bool traverseTest(TREE::iterator *beginIter, std::string str) {
    //Create buffer in which to redirect std::cout to
    std::stringstream buffer;

    // Save cout's buffer here
    std::streambuf *sbuf = std::cout.rdbuf();

    std::cout.flush ();
    // Redirect cout to the stringstream buffer
    std::cout.rdbuf(buffer.rdbuf());

    Print_Visitor visitor;

    for (std::auto_ptr<TREE::iterator> iter (beginIter); !iter->done (); iter->advance ())
        iter->current_item ().accept (visitor);

//    EXPECT_NO_THROW (strategy->traverse (*tree));
    std::cout.flush ();
    // Redirect cout back to its old self
    std::cout.rdbuf(sbuf);


    char c;
    //std::cout << "\nGot traversal: ";
    for(std::string::iterator i = str.begin(); i != str.end(); i++ ) {
        c = buffer.get();

        if (c == EOF)
          {
            std::cout << std::endl << "Reached the end of input.\n";
            return false;
          }
        //std::cout << c;
        if(*i != c) {
          std::cout<<std::endl<<"Failed Traversal For One Node.";
          std::cout<<std::endl<<"Make sure you are not printing spaces between the items in each Node.";
          std::cout<<std::endl<<"\nIn the Tree String "<<str;

          std::cout<<std::endl<<"Expected "<< *i;
          std::cout<<std::endl<<"Actual "<< c << std::endl;

         // Commented this so that students can see the whole string and debug it
            //return false;
        }
    }

 return true;
}
#endif

TEST(Singleton, OptionsInstance) {
   atexit (cleanup_options);
   Options *o1 = Options::instance();

   {
       Options *o2 = Options::instance();
       EXPECT_EQ(o1, o2);
   }

   Options *o3 = Options::instance();
   EXPECT_EQ(o1, o3);
}


TEST(MakeSimpleTree, DefaultQueueType) {
    TREE tree = MAKE_TREE;
}

TEST(MakeLeaningTree, DefaultQueueType) {
    TREE tree = make_leaning_tree();
}

TEST(TraverseSimpleTree, DefaultQueueType) {

  TREE tree = MAKE_TREE;

  EXPECT_EQ("Levelorder", Options::instance ()->traversal_strategy ());
  EXPECT_EQ("LQueue", Options::instance ()->queue_type ());

#if GRADUATE_STUDENT == 1
//Non-const iterators
    EXPECT_NO_THROW(TREE::iterator itr1 = tree.begin (Options::instance ()->traversal_strategy ()));
    EXPECT_NO_THROW(TREE::iterator itr2 = tree.end (Options::instance ()->traversal_strategy ()));

    EXPECT_PRED2(traverseTest, &tree, "*+-1579");

#else
    TREE::iterator *itr;
    EXPECT_NO_THROW(itr = tree.create_iterator (Options::instance ()->traversal_strategy ()));

    EXPECT_PRED2(traverseTest, itr, "+-*3475");
#endif
}

TEST(TraverseLeaningTree, DefaultQueueType) {

  TREE tree = make_leaning_tree();

  EXPECT_EQ("Levelorder", Options::instance ()->traversal_strategy ());
  EXPECT_EQ("LQueue", Options::instance ()->queue_type ());

#if GRADUATE_STUDENT == 1
//Non-const iterators
    EXPECT_NO_THROW(TREE::iterator itr1 = tree.begin (Options::instance ()->traversal_strategy ()));
    EXPECT_NO_THROW(TREE::iterator itr2 = tree.end (Options::instance ()->traversal_strategy ()));

    EXPECT_PRED2(traverseTest, &tree, "+*/13*957");

#else
    TREE::iterator *itr;
    EXPECT_NO_THROW(itr = tree.create_iterator (Options::instance ()->traversal_strategy ()));

    EXPECT_PRED2(traverseTest, itr, "+*/13*957");
#endif

}


 TEST(BadStrategy, TraversalType)
   {
#if GRADUATE_STUDENT == 1
     EXPECT_THROW (MAKE_TRAVERSAL_STRATEGY ("Asdf"),
                   Tree_Iterator_Impl<char>::Unknown_Order);
#else
     EXPECT_THROW (MAKE_TRAVERSAL_STRATEGY ("Asdf"),
                   Unknown_Strategy);
#endif
   }

 TEST(BadStrategy, QueueType)
   {
     Options *opts = Options::instance ();
     opts->queue_type_ = "asdf";
#if GRADUATE_STUDENT == 1
     EXPECT_THROW (MAKE_TRAVERSAL_STRATEGY ("Levelorder"),
                   Tree_Iterator_Impl<char>::Unknown_Order);
#else
     EXPECT_THROW (MAKE_TRAVERSAL_STRATEGY ("Asdf"),
                   Unknown_Strategy);
#endif
}


TEST(LevelOrderSimpleTree, AQueue) {
    parsing::optind = 0;
    const char *argv[] = { "tree-traversal", "-t", "L", "-q", "A"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Levelorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("AQueue", Options::instance ()->queue_type ());

   //     TRAVERSAL_HOLDER traversal_strategy;
   // EXPECT_NO_THROW (traversal_strategy.reset (
   //   MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));

    TREE tree = MAKE_TREE;

#if GRADUATE_STUDENT == 1
//Non-const iterators
    EXPECT_NO_THROW(TREE::iterator itr1 = tree.begin (Options::instance ()->traversal_strategy ()));
    EXPECT_NO_THROW(TREE::iterator itr2 = tree.end (Options::instance ()->traversal_strategy ()));

    EXPECT_PRED2(traverseTest, &tree, "*+-1579");

#else
    TREE::iterator *itr;
    EXPECT_NO_THROW(itr = tree.create_iterator (Options::instance ()->traversal_strategy ()));

    EXPECT_PRED2(traverseTest, itr, "+-*3475");
#endif
}

TEST(LevelOrderSimpleTree, LQueue) {
    parsing::optind = 0;
    const char *argv[] = { "tree-traversal", "-t", "L", "-q", "L"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Levelorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("LQueue", Options::instance ()->queue_type ());

   //     TRAVERSAL_HOLDER traversal_strategy;
   // EXPECT_NO_THROW (traversal_strategy.reset (
   //   MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));

    TREE tree = MAKE_TREE;
#if GRADUATE_STUDENT == 1
//Non-const iterators
    EXPECT_NO_THROW(TREE::iterator itr1 = tree.begin (Options::instance ()->traversal_strategy ()));
    EXPECT_NO_THROW(TREE::iterator itr2 = tree.end (Options::instance ()->traversal_strategy ()));

    EXPECT_PRED2(traverseTest, &tree, "*+-1579");
#else
    TREE::iterator *itr;
    EXPECT_NO_THROW(itr = tree.create_iterator (Options::instance ()->traversal_strategy ()));

    EXPECT_PRED2(traverseTest, itr, "+-*3475");
#endif
}

TEST(PreOrderSimpleTree, AQueue) {
    parsing::optind = 0;
    const char *argv[] = { "tree-traversal", "-t", "P", "-q", "A"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Preorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("AQueue", Options::instance ()->queue_type ());

   //     TRAVERSAL_HOLDER traversal_strategy;
   // EXPECT_NO_THROW (traversal_strategy.reset (
   //   MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));

    TREE tree = MAKE_TREE;

#if GRADUATE_STUDENT == 1
//Non-const iterators
    EXPECT_THROW(TREE::iterator itr1 = tree.begin (Options::instance ()->traversal_strategy ()), Tree_Iterator_Impl<char>::Unknown_Order);
    EXPECT_THROW(TREE::iterator itr2 = tree.end (Options::instance ()->traversal_strategy ()), Tree_Iterator_Impl<char>::Unknown_Order);

    //EXPECT_PRED2(traverseTest, &tree, "*+-1579");

#else

    TREE::iterator *itr;
    EXPECT_THROW(itr = tree.create_iterator (Options::instance ()->traversal_strategy ()), Unknown_Strategy);
#endif
}

TEST(PreOrderSimpleTree, LQueue) {
    parsing::optind = 0;
    const char *argv[] = { "tree-traversal", "-t", "P", "-q", "L"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Preorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("LQueue", Options::instance ()->queue_type ());

   //     TRAVERSAL_HOLDER traversal_strategy;
   // EXPECT_NO_THROW (traversal_strategy.reset (
   //   MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));

    TREE tree = MAKE_TREE;

#if GRADUATE_STUDENT == 1
//Non-const iterators
    EXPECT_THROW(TREE::iterator itr1 = tree.begin (Options::instance ()->traversal_strategy ()), Tree_Iterator_Impl<char>::Unknown_Order);
    EXPECT_THROW(TREE::iterator itr2 = tree.end (Options::instance ()->traversal_strategy ()), Tree_Iterator_Impl<char>::Unknown_Order);

    //EXPECT_PRED2(traverseTest, &tree, "*+-1579");
#else

    TREE::iterator *itr;
    EXPECT_THROW(itr = tree.create_iterator (Options::instance ()->traversal_strategy ()), Unknown_Strategy);
#endif
}

TEST(InOrderSimpleTree, AQueue) {
    parsing::optind = 0;
    const char *argv[] = { "tree-traversal", "-t", "I", "-q", "A"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Inorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("AQueue", Options::instance ()->queue_type ());

   //     TRAVERSAL_HOLDER traversal_strategy;
   // EXPECT_NO_THROW (traversal_strategy.reset (
   //   MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));

    TREE tree = MAKE_TREE;

#if GRADUATE_STUDENT == 1
//Non-const iterators
    EXPECT_THROW(TREE::iterator itr1 = tree.begin (Options::instance ()->traversal_strategy ()), Tree_Iterator_Impl<char>::Unknown_Order);
    EXPECT_THROW(TREE::iterator itr2 = tree.end (Options::instance ()->traversal_strategy ()), Tree_Iterator_Impl<char>::Unknown_Order);

    //EXPECT_PRED2(traverseTest, &tree, "*+-1579");
#else

    TREE::iterator *itr;
    EXPECT_THROW(itr = tree.create_iterator (Options::instance ()->traversal_strategy ()), Unknown_Strategy);
#endif
}

TEST(InOrderSimpleTree, LQueue) {
    parsing::optind = 0;
    const char *argv[] = { "tree-traversal", "-t", "I", "-q", "L"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Inorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("LQueue", Options::instance ()->queue_type ());

   //     TRAVERSAL_HOLDER traversal_strategy;
   // EXPECT_NO_THROW (traversal_strategy.reset (
   //   MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));

    TREE tree = MAKE_TREE;

#if GRADUATE_STUDENT == 1
//Non-const iterators
    EXPECT_THROW(TREE::iterator itr1 = tree.begin (Options::instance ()->traversal_strategy ()), Tree_Iterator_Impl<char>::Unknown_Order);
    EXPECT_THROW(TREE::iterator itr2 = tree.end (Options::instance ()->traversal_strategy ()), Tree_Iterator_Impl<char>::Unknown_Order);

    //EXPECT_PRED2(traverseTest, &tree, "*+-1579");
#else

    TREE::iterator *itr;
    EXPECT_THROW(itr = tree.create_iterator (Options::instance ()->traversal_strategy ()), Unknown_Strategy);
#endif
}

TEST(PostOrderSimpleTree, AQueue) {
    parsing::optind = 0;
    const char *argv[] = { "tree-traversal", "-t", "O", "-q", "A"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Postorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("AQueue", Options::instance ()->queue_type ());

   //     TRAVERSAL_HOLDER traversal_strategy;
   // EXPECT_NO_THROW (traversal_strategy.reset (
   //   MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));

    TREE tree = MAKE_TREE;

#if GRADUATE_STUDENT == 1
//Non-const iterators
    EXPECT_THROW(TREE::iterator itr1 = tree.begin (Options::instance ()->traversal_strategy ()), Tree_Iterator_Impl<char>::Unknown_Order);
    EXPECT_THROW(TREE::iterator itr2 = tree.end (Options::instance ()->traversal_strategy ()), Tree_Iterator_Impl<char>::Unknown_Order);

    //EXPECT_PRED2(traverseTest, &tree, "*+-1579");
#else

    TREE::iterator *itr;
    EXPECT_THROW(itr = tree.create_iterator (Options::instance ()->traversal_strategy ()), Unknown_Strategy);
#endif
}

TEST(PostOrderSimpleTree, LQueue) {
    parsing::optind = 0;
    const char *argv[] = { "tree-traversal", "-t", "O", "-q", "L"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Postorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("LQueue", Options::instance ()->queue_type ());

   //     TRAVERSAL_HOLDER traversal_strategy;
   // EXPECT_NO_THROW (traversal_strategy.reset (
   //   MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));

    TREE tree = MAKE_TREE;

#if GRADUATE_STUDENT == 1
//Non-const iterators
    EXPECT_THROW(TREE::iterator itr1 = tree.begin (Options::instance ()->traversal_strategy ()), Tree_Iterator_Impl<char>::Unknown_Order);
    EXPECT_THROW(TREE::iterator itr2 = tree.end (Options::instance ()->traversal_strategy ()), Tree_Iterator_Impl<char>::Unknown_Order);

    //EXPECT_PRED2(traverseTest, &tree, "*+-1579");
#else

    TREE::iterator *itr;
    EXPECT_THROW(itr = tree.create_iterator (Options::instance ()->traversal_strategy ()), Unknown_Strategy);
#endif
}

/*********************************LEANING TREE**********************************/

TEST(LevelOrderLeaningTree, AQueue) {
    parsing::optind = 0;
    const char *argv[] = { "tree-traversal", "-t", "L", "-q", "A"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Levelorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("AQueue", Options::instance ()->queue_type ());

   //     TRAVERSAL_HOLDER traversal_strategy;
   // EXPECT_NO_THROW (traversal_strategy.reset (
   //   MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));

    TREE tree = make_leaning_tree();

#if GRADUATE_STUDENT == 1
//Non-const iterators
    EXPECT_NO_THROW(TREE::iterator itr1 = tree.begin (Options::instance ()->traversal_strategy ()));
    EXPECT_NO_THROW(TREE::iterator itr2 = tree.end (Options::instance ()->traversal_strategy ()));

    EXPECT_PRED2(traverseTest, &tree, "+*/13*957");
#else
    TREE::iterator *itr;
    EXPECT_NO_THROW(itr = tree.create_iterator (Options::instance ()->traversal_strategy ()));

    EXPECT_PRED2(traverseTest, itr, "+*/13*957");
#endif
}

TEST(LevelOrderLeaningTree, LQueue) {
    parsing::optind = 0;
    const char *argv[] = { "tree-traversal", "-t", "L", "-q", "L"};

    Options::instance ()->parse_args (argc, (char **)argv);

    EXPECT_EQ("Levelorder", Options::instance ()->traversal_strategy ());
    EXPECT_EQ("LQueue", Options::instance ()->queue_type ());

   //     TRAVERSAL_HOLDER traversal_strategy;
   // EXPECT_NO_THROW (traversal_strategy.reset (
   //   MAKE_TRAVERSAL_STRATEGY (Options::instance ()->traversal_strategy ())));

    TREE tree = make_leaning_tree();

#if GRADUATE_STUDENT == 1
//Non-const iterators
    EXPECT_NO_THROW(TREE::iterator itr1 = tree.begin (Options::instance ()->traversal_strategy ()));
    EXPECT_NO_THROW(TREE::iterator itr2 = tree.end (Options::instance ()->traversal_strategy ()));

    EXPECT_PRED2(traverseTest, &tree, "+*/13*957");
#else
    TREE::iterator *itr;
    EXPECT_NO_THROW(itr = tree.create_iterator (Options::instance ()->traversal_strategy ()));

    EXPECT_PRED2(traverseTest, itr, "+*/13*957");
#endif

}
