#include <iostream>
#include <memory>
#include <algorithm>

#include "Tree.h"
#include "Factories.h"
#include "Options.h"
#include "Print_Visitor.h"

int 
main (int argc, char *argv[])
{
  try 
    {
      // Create options class to parse command line options.
      std::auto_ptr<Options> options (Options::instance ());

      // Parse the command-line options. If the user requested help (via "-h"
      // or "-?") then print out the usage and return.
      if (!Options::instance ()->parse_args (argc, argv))
        return 0;

      std::cout << "--Testing options class (singleton)--\n\n";

      // Print out the options used.
      std::cout << "  traversal option: " << options->traversal_strategy ()
                << std::endl;
      std::cout << "  queue option: " << options->queue_type ()
                << std::endl << std::endl;

      // Use the factory to create the tree.
      TREE root_node = make_tree ();

      std::cout << "Testing the Print_Visitor: " << std::endl;

      Print_Visitor visitor;

      for (std::auto_ptr<TREE::iterator> iter (root_node.create_iterator (Options::instance ()->traversal_strategy ()));
           !iter->done ();
           iter->advance ())
        iter->current_item ().accept (visitor);

      std::cout << std::endl;
    }
  catch (...)
    {
      std::cout << "some exception occurred" << std::endl;
    }

  return 0;
}
