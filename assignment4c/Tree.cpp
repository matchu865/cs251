/* -*- C++ -*- */

/***************************************
Class: CS251
Assignment Number: 4c
Honor Pledge: I pledge that I have not recieved nor given help on this assignment.
***************************************/

#if !defined (_TREE_CPP)
#define _TREE_CPP

#include "Tree.h"
#include "Tree_Iterator_Impl.h"
#include "Print_Visitor.h"

// Default ctor
template <typename T>
Tree<T>::Tree ()
{
}

// Ctor take an underlying Node<T>*.
template <typename T>
Tree<T>::Tree (Node<T> *root, bool count)
  : root_ (root, count)
{    
}

// Ctor taking an item to store.
template <typename T>
Tree<T>::Tree (const T &item)
: root_ (new Node<T>(item))
{
}

// Ctor taking item and two children.
template <typename T>
Tree<T>::Tree (const T &item, Node<T> *left, Node<T> *right)
  : root_ (new Node<T>(item, left, right))
{
}

// Copy ctor
template <typename T>
Tree<T>::Tree (const Tree &t)
  : root_ (t.root_)
{
}

// Assignment operator
template <typename T>
void
Tree<T>::operator= (const Tree &t)
{
  if (this != &t)
    root_ = t.root_;
}

// Dtor
template <typename T>
Tree<T>::~Tree ()
{
}

// Check if the tree is empty.
template <typename T>
bool
Tree<T>::is_null (void) const
{
  return root_.is_null();
}

// Return the stored item.
template <typename T>
const T &
Tree<T>::item (void) const
{
  return root_->item();
}

// Return the left branch.
template <typename T>
Tree<T>
Tree<T>::left (void) const
{
  // wrap the Node* in a tree object and increase reference count by one.
  return Tree<T>(root_->left(), true);
}

// Return the left branch.
template <typename T>
Tree<T>
Tree<T>::right (void) const
{
  // wrap the Node* in a tree object and increase reference count by one.
  return Tree<T>(root_->right(), true);
}

template <typename T>
typename Tree<T>::iterator *
Tree<T>::create_iterator (const std::string &type)
{
	if(type == "Levelorder")
		return new Level_Order_Tree_Iterator_Impl(*this);
	else
		throw Unknown_Strategy(type);
}

template <typename T>
void 
Tree<T>::accept(Visitor &v)
{
  v.visit(*this->root_);
}

#endif /* _TREE_CPP */
