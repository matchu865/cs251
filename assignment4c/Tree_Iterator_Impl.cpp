/* -*- C++ -*- */

/***************************************
Class: CS251
Assignment Number: 4c
Honor Pledge: I pledge that I have not recieved nor given help on this assignment.
***************************************/

#if !defined (_TREE_ITERATOR_IMPL_CPP)
#define _TREE_ITERATOR_IMPL_CPP

#include <string>

#include "AQueue.h"
#include "LQueue.h"
#include "Node.h"
#include "Print_Visitor.h"
#include "Tree.h"
#include "Options.h"
#include "Tree_Iterator_Impl.h"

// Destructor
Tree_Iterator_Impl::~Tree_Iterator_Impl()
{
}

// Constructor
Level_Order_Tree_Iterator_Impl::Level_Order_Tree_Iterator_Impl(const TREE &tree):
// You fill in here.
myqueue_()
{
	make_queue_strategy(Options::instance()->queue_type());
        // @@ check if tree is nill!
	if(!tree.is_null())
	  myqueue_->enqueue(tree);
}

// Destructor
Level_Order_Tree_Iterator_Impl::~Level_Order_Tree_Iterator_Impl(void)
{
  //noop
}

// Dereferencing
TREE
Level_Order_Tree_Iterator_Impl::current_item (void)
{
	return myqueue_->front();
}

// Increment the iterator.
void
Level_Order_Tree_Iterator_Impl::advance (void)
{
  // @@ Only if not done!
    if(!done()){
		TREE tree = myqueue_->front();
		if(!(tree.left().is_null()))
			myqueue_->enqueue(tree.left());
		if(!(tree.right().is_null()))
			myqueue_->enqueue(tree.right());
		myqueue_->dequeue();
    }
}

bool
Level_Order_Tree_Iterator_Impl::done (void)
{
	return myqueue_->is_empty();
}

void
Level_Order_Tree_Iterator_Impl::make_queue_strategy (const std::string &queue_type)
{
    if(queue_type == "AQueue"){
	   myqueue_ = std::unique_ptr<QUEUE>(new AQUEUE_ADAPTER(80));
	   return;
    }
    if(queue_type == "LQueue"){
	   myqueue_ = std::unique_ptr<QUEUE>(new LQUEUE_ADAPTER(0));
      return;
    }
    throw Unknown_Strategy(queue_type);
}

#endif /* _TREE_ITERATOR_IMPL_CPP */
