/* -*- C++ -*- */

/***************************************
Class: CS251
Assignment Number: 4c
Honor Pledge: I pledge that I have not recieved nor given help on this assignment.
***************************************/

#ifndef _NODE_H
#define _NODE_H

#include "Refcounter.h"
#include "Typedefs.h"

/**
 * @class Node
 * @brief Defines a very simple implementation of a binary tree node.
 */
template <typename T>
class Node
{
  /// Needed for reference counting.
  friend class Tree<T>;
  friend class Refcounter<Node<T> >;

public:
  /// Ctor
  Node (const T &item, Node<T> *left = 0, Node<T> *right = 0);

  /// Dtor
  virtual ~Node ();

  /// Return the item stored in the node.
  const T &item (void) const;

  /// Return the left child.
  Node<T> *left (void) const;

  /// Return the right child.
  Node<T> *right (void) const;

  // Visitor pattern
  void accept (Visitor &v);

private:
  /// Item stored in the node.
  T item_;

  /// Left child.
  Node<T> *left_;

  /// Right child.
  Node<T> *right_;

  /// Reference counter
  int use_;
};

#include "Node.cpp"

#endif /* _NODE_H */
