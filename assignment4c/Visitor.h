/* -*- C++ -*- */

/***************************************
Class: CS251
Assignment Number: 4c
Honor Pledge: I pledge that I have not recieved nor given help on this assignment.
***************************************/

#ifndef _VISITOR_H
#define _VISITOR_H

#include "Typedefs.h"

/**
 * @class Visitor
 * @brief Defines a Visitor interface for subclasses.
 */
class Visitor
{
public:
    // Visit the NODE.
  virtual void visit (NODE &tree) = 0;

  // Virtual destructor.
  virtual ~Visitor () = 0;
};

#endif /* _VISITOR_H */
