#include "ArrayTestUtils.h"

std::ostream& operator<<(std::ostream& os, const ARRAY& a) {
    os << "[" << a.size() << "] { ";
    std::copy(cbegin(a), cend(a), std::ostream_iterator<char>(os, " "));
    os << "}";
    return os;
}

void PrintTo(const ARRAY& a, std::ostream* os) {
    *os << a;
}

std::ostream& operator<<(std::ostream& os, const vector& v) {
    os << "[" << v.size() << "] { ";
    std::copy(v.cbegin(), v.cend(), std::ostream_iterator<char>(os, " "));
    os << "}";
    return os;
}

testing::AssertionResult equalWithIterators(const vector& v, const ARRAY& a) {
    if (a.size() != v.size())
        return testing::AssertionFailure()
             << "size mismatch " << v << " vs " << a;

    if (std::equal(cbegin(a), cend(a), v.cbegin()))
        return testing::AssertionSuccess();

    return testing::AssertionFailure() << "value mismatch " << v << " vs " << a;
}

testing::AssertionResult equalWithGet(const vector& v, const ARRAY& a) {
    if (a.size() != v.size())
        return testing::AssertionFailure()
             << "size mismatch " << v << " vs " << a;

    try {
        char x;
        for (size_t i = 0; i < a.size(); ++i) {
            a.get(x, i);
            if (x != v[i])
                throw "mismatch";
        }
    } catch (...) {
        return testing::AssertionFailure() << "value mismatch " << v << " vs " << a;
    }

    return testing::AssertionSuccess();
}

testing::AssertionResult equalWithSubscript(const vector& v, const ARRAY& a) {
    if (a.size() != v.size())
        return testing::AssertionFailure()
            << "size mismatch " << v << " vs " << a;

    for (size_t i = 0; i < a.size(); ++i) {
        if (a[i] != v[i])
            return testing::AssertionFailure()
                << "value mismatch " << v << " vs " << a;
    }

    return testing::AssertionSuccess();
}

#if GRADUATE_STUDENT == 0
char* begin(ARRAY& a) {
    return &a[0];
}

char* end(ARRAY& a) {
    return begin(a) + a.size();
}

const char* cbegin(const ARRAY& a) {
    return &a[0];
}

const char* cend(const ARRAY& a) {
    return cbegin(a) + a.size();
}

#else

ARRAY::iterator begin(ARRAY& a)
{
return a.begin ();
}

ARRAY::iterator end(ARRAY& a)
{
return a.end ();
}

ARRAY::const_iterator cbegin(const ARRAY& a)
{
return a.begin ();
}

ARRAY::const_iterator cend(const ARRAY& a)
{
return a.end ();
}
#endif
