#include "ArrayTestUtils.h"
#include <stdexcept>

const vector tenZeros(10, '0');
const vector zeroNineVec{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

#if GRADUATE_STUDENT == 1

TEST(Iterators, Const){
  const ARRAY a1 (20,'t');
  ARRAY::const_iterator i1 = a1.begin ();
  ARRAY::const_iterator i2 = a1.end ();

  size_t i = 0;

  for (i = 0; i1 != a1.end (); ++i1, ++i)
  {
    EXPECT_EQ('t', *i1);
  }

  EXPECT_EQ(20u, i);

  for (i = 0; i2 != a1.begin (); ++i)
  {
    EXPECT_EQ('t', *--i2);
  }

  //Check if exactly 20 iterations occurred
  EXPECT_EQ(20u, i);

}

TEST(Iterators, NonConst) {
  ARRAY a1 (20,'t');
  ARRAY::iterator i1 = a1.begin ();
  ARRAY::iterator i2 = a1.end ();

  size_t i = 0;

  for (i = 0; i1 != a1.end (); ++i1, ++i)
  {
    EXPECT_EQ('t', *i1);
    *i1 = 'r';
  }

  EXPECT_EQ(20u, i);

  for (i = 0; i2 != a1.begin (); ++i)
  {
    EXPECT_EQ('r', *--i2);
  }

  //Check if exactly 20 iterations occurred
  EXPECT_EQ(20u, i);
}

#endif

TEST(SetDefaults, Simple) {
  ARRAY a1 (10);
  ARRAY a2 (10,'e');
  ARRAY a3 (20);
  //Test if the assignment operator takes over the default settings
  a3 = a2;

  std::fill(begin (a1), end (a1), 'e');
  a1.set ('u', 15);
  a2.set ('u', 15);
  a3.set ('u', 15);

  EXPECT_EQ(16u, a1.size());
  EXPECT_EQ(16, std::distance(begin(a1), end(a1)));
  EXPECT_EQ(16, std::distance(cbegin(a1), cend(a1)));

  for (size_t i = 0; i < a1.size(); i++) {

      if(15 == i) {
          //Only the last 16th value will be a 'u'
          EXPECT_EQ('u', a1[i]);
      }
      else if(i > 9 && i < 15) {
        // garbage
      }
      else {
          //All other values from index 0-9 will be 'e's
          EXPECT_EQ('e', a1[i]);
      }
  }

  EXPECT_EQ(16u, a2.size());
  EXPECT_EQ(16, std::distance(begin(a2), end(a2)));
  EXPECT_EQ(16, std::distance(cbegin(a2), cend(a2)));

  for (size_t i = 0; i < a2.size(); i++) {

      if(15 == i) {
          //Only the last 16th value will be a 'u'
          EXPECT_EQ('u', a2[i]);
      }
      else {
          //All other values from index 0-14 will be 'e's
          EXPECT_EQ('e', a2[i]);
      }
  }


  EXPECT_EQ(16u, a3.size());
  EXPECT_EQ(16, std::distance(begin(a3), end(a3)));
  EXPECT_EQ(16, std::distance(cbegin(a3), cend(a3)));

  for (size_t i = 0; i < a3.size(); i++) {

      if(15 == i) {
          //Only the last 16th value will be a 'u'
          EXPECT_EQ('u', a3[i]);
      }
      else {
          //All other values from index 0-14 will be 'e's
          EXPECT_EQ('e', a3[i]);
      }
  }
}

TEST(Swap, Simple) {

  ARRAY a1 (10, 'a');
  ARRAY a2 (10, 'b');
  ARRAY a3 (10, 'a');
  ARRAY a4 (10, 'b');

  EXPECT_NE (&a1[0], &a2[0]);
  EXPECT_EQ (a1, a3);
  EXPECT_EQ (a2, a4);
  a1.swap (a2);
  EXPECT_NE (&a1[0], &a2[0]);
  EXPECT_EQ (a1, a4);
  EXPECT_EQ (a2, a3);
}

TEST(SetResize, Simple) {
  ARRAY a1 (10);

  ARRAY::value_type val;

  EXPECT_THROW (a1.get(val, 10), std::out_of_range);
  a1.set ('d', 10);

  a1.get (val, 10);
  EXPECT_EQ (val, 'd');
}

TEST(SetAndSubscript, Set) {
    ARRAY a(10, '0');
    EXPECT_PRED2(equalWithGet, tenZeros, a);

    for (size_t i = 0; i < 10u; ++i)
        a.set(i + '0', i);
    EXPECT_PRED2(equalWithGet, zeroNineVec, a);
}

TEST(SetAndSubscript, Subscript) {
    ARRAY a(10, '0');
    EXPECT_PRED2(equalWithSubscript, tenZeros, a);

    for (size_t i = 0; i < 10u; ++i)
        a[i] = i + '0';
    EXPECT_PRED2(equalWithSubscript, zeroNineVec, a);
}

TEST(EdgeConditions, SequentialStorage) {
    ARRAY a(2);
    EXPECT_EQ(1, &a[1] - &a[0]);
}

TEST(EdgeConditions, AssignmentChaining) {
    ARRAY a(10), b(20), c(30);
    ARRAY* aPtr = &a;
    EXPECT_EQ(aPtr, &(a = b = c));
}

TEST(EdgeConditions, GetFailsCorrectly) {
    ARRAY a(10);

    char shouldFail;
    EXPECT_THROW(a.get(shouldFail, 10), std::out_of_range);
}
