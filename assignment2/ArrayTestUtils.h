#ifndef _ARRAY_TEST_UTILS_H_
#define _ARRAY_TEST_UTILS_H_

#define GRADUATE_STUDENT 0

#include "Array.h"
#include "gtest/gtest.h"
#include <vector>
#include <iostream>
#include <algorithm>

typedef Array<char> ARRAY;

typedef std::vector<char> vector;

std::ostream& operator<<(std::ostream&, const ARRAY&);

void PrintTo(const ARRAY&, std::ostream*);

std::ostream& operator<<(std::ostream&, const vector&);

testing::AssertionResult equalWithIterators(const vector&, const ARRAY&);

testing::AssertionResult equalWithGet(const vector&, const ARRAY&);

testing::AssertionResult equalWithSubscript(const vector&, const ARRAY&);

#if GRADUATE_STUDENT == 0
char* begin(ARRAY& a);

char* end(ARRAY& a);

const char* cbegin(const ARRAY& a);

const char* cend(const ARRAY& a);

#else

ARRAY::iterator begin(ARRAY& a);

ARRAY::iterator end(ARRAY& a);

ARRAY::const_iterator cbegin(const ARRAY& a);

ARRAY::const_iterator cend(const ARRAY& a);

#endif

#endif
