#include "ArrayTestUtils.h"
#include <stdexcept>
#include <iterator>


const vector tenAs(10, 'a');
const vector twentyBs(20, 'b');
const vector thirtyCs(30, 'c');

/*
const vector emptyVec;
const vector tenZeros(10, '0');
const vector tenOnes(10, '1');
const vector oneFiveVec{'1', '2', '3', '4', '5'};
*/

TEST(CopyConstructor, Trivial) {

  ARRAY a1 (10, 'a');
  ARRAY a2 (20, 'b');
  ARRAY a3 (30, 'c');

  // std::cout << "\n\n-----Performing copy constructor tests-----\n\n";

  ARRAY b1(a1);
  ARRAY b2(a2);
  ARRAY b3(a3);
  ARRAY c4 (30);
  ARRAY c5 (c4);

  // std::cout << "\nTesting b1(a1). a1 contained 10 a's.\n\n";

  EXPECT_PRED2(equalWithIterators, tenAs, b1);
  EXPECT_PRED2(equalWithGet, tenAs, b1);
  EXPECT_PRED2(equalWithSubscript, tenAs, b1);
  EXPECT_EQ(a1, b1);

  EXPECT_NE(begin(a1), begin(b1));
  EXPECT_NE(cbegin(a1), cbegin(b1));
  EXPECT_NE(end(a1), end(b1));
  EXPECT_NE(cend(a1), cend(b1));

  // std::cout << "\nTesting b2(a2). a2 contained 20 b's.\n\n";

  EXPECT_PRED2(equalWithIterators, twentyBs, b2);
  EXPECT_PRED2(equalWithGet, twentyBs, b2);
  EXPECT_PRED2(equalWithSubscript, twentyBs, b2);
  EXPECT_EQ(a2, b2);

  EXPECT_NE(begin(a2), begin(b2));
  EXPECT_NE(cbegin(a2), cbegin(b2));
  EXPECT_NE(end(a2), end(b2));
  EXPECT_NE(cend(a2), cend(b2));

  // std::cout << "\nTesting b3(a3). b3 contained 30 c's.\n\n";

  EXPECT_PRED2(equalWithIterators, thirtyCs, b3);
  EXPECT_PRED2(equalWithGet, thirtyCs, b3);
  EXPECT_PRED2(equalWithSubscript, thirtyCs, b3);
  EXPECT_EQ(a3, b3);

  EXPECT_NE(begin(a3), begin(b3));
  EXPECT_NE(cbegin(a3), cbegin(b3));
  EXPECT_NE(end(a3), end(b3));
  EXPECT_NE(cend(a3), cend(b3));

  // std::cout << "\nTesting c5(c4).\n\n";

 /* EXPECT_EQ(c4.max_size_, c5.max_size_);
  EXPECT_EQ(c4.cur_size_, c5.cur_size_);*/
  EXPECT_EQ(c4.size(), c5.size());
  EXPECT_EQ(c4, c5);

  EXPECT_NE(begin(c4), begin(c5));
  EXPECT_NE(cbegin(c4), cbegin(c5));
  EXPECT_NE(end(c4), end(c5));
  EXPECT_NE(cend(c4), cend(c5));
}
/******************************************8
TEST(CopyConstructor, EmptyArrayConsistency) {
    ARRAY empty(0);
    ARRAY copy(empty);

    EXPECT_EQ(0u, copy.size());
    char shouldFail;
#if GRADUATE_STUDENT == 0
    EXPECT_EQ(-1, copy.get(shouldFail, 0));
#else
    EXPECT_THROW(copy.get(shouldFail, 0), std::out_of_range);
#endif

    EXPECT_PRED2(equalWithIterators, emptyVec, copy);
    EXPECT_PRED2(equalWithGet, emptyVec, copy);
    EXPECT_PRED2(equalWithSubscript, emptyVec, copy);

    EXPECT_EQ(begin(empty), end(empty));
    EXPECT_EQ(cbegin(empty), cend(empty));
    EXPECT_EQ(begin(copy), end(copy));
    EXPECT_EQ(cbegin(copy), cend(copy));

    if ((cbegin(empty) == nullptr && cbegin(copy) != nullptr)
            || (cbegin(empty) != nullptr && cbegin(copy) == nullptr))
        FAIL() << "Inconsistent empty Array behavior";
}
*********************************************************/

TEST(Assignment, Simple) {

  ARRAY a1 (10, 'a');
  ARRAY a2 (20, 'b');
  ARRAY a3 (30, 'c');
  ARRAY c1 (1, 'x');
  ARRAY c2 (1, 'y');
  ARRAY c3 (1, 'z');


  // std::cout << "\n\n-----Performing assignment operator tests-----\n\n";

  c1 = a1;
  c2 = a2;
  c3 = a3;

  // std::cout << "\nTesting c1 = a1. c1 should contain 10 a's.\n\n";
  EXPECT_PRED2(equalWithIterators, tenAs, c1);
  EXPECT_PRED2(equalWithGet, tenAs, c1);
  EXPECT_PRED2(equalWithSubscript, tenAs, c1);
  EXPECT_EQ(a1, c1);

  EXPECT_NE(begin(a1), begin(c1));
  EXPECT_NE(cbegin(a1), cbegin(c1));
  EXPECT_NE(end(a1), end(c1));
  EXPECT_NE(cend(a1), cend(c1));

  // std::cout << "\nTesting c2 = a2. c2 should contain 20 b's.\n\n";
  EXPECT_PRED2(equalWithIterators, twentyBs, c2);
  EXPECT_PRED2(equalWithGet, twentyBs, c2);
  EXPECT_PRED2(equalWithSubscript, twentyBs, c2);
  EXPECT_EQ(a2, c2);

  EXPECT_NE(begin(a2), begin(c2));
  EXPECT_NE(cbegin(a2), cbegin(c2));
  EXPECT_NE(end(a2), end(c2));
  EXPECT_NE(cend(a2), cend(c2));

  // std::cout << "\nTesting c3 = a3. c3 should contain 30 c's.\n\n";
  EXPECT_PRED2(equalWithIterators, thirtyCs, c3);
  EXPECT_PRED2(equalWithGet, thirtyCs, c3);
  EXPECT_PRED2(equalWithSubscript, thirtyCs, c3);
  EXPECT_EQ(a3, c3);

  EXPECT_NE(begin(a3), begin(c3));
  EXPECT_NE(cbegin(a3), cbegin(c3));
  EXPECT_NE(end(a3), end(c3));
  EXPECT_NE(cend(a3), cend(c3));



  // std::cout << "\n----- Assignments/constructors" <<
  //   " have been tested. Please review results.-----\n\n";


/*
    Array a(0);
    a = a;
    EXPECT_PRED2(equalWithIterators, emptyVec, a);
    EXPECT_PRED2(equalWithGet, emptyVec, a);
    EXPECT_PRED2(equalWithSubscript, emptyVec, a);

    a = oneFive;
    EXPECT_EQ(a, oneFive);
    EXPECT_PRED2(equalWithIterators, oneFiveVec, a);
    EXPECT_PRED2(equalWithGet, oneFiveVec, a);
    EXPECT_PRED2(equalWithSubscript, oneFiveVec, a);
*/
}
