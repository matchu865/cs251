#include "ArrayTestUtils.h"
#include <stdexcept>
#include <iterator>


const vector tenAs(10, 'a');
const vector twentyBs(20, 'b');
const vector thirtyCs(30, 'c');


TEST(Constructor, Defaults) {
    ARRAY a1 (10, 'a'), a2 (20, 'b'),a3 (30, 'c');
    //Testing defaults for a1
    // std::cout << "\n-----Entering default value tests-----\n\n";

    // std::cout << "Testing defaults for a1 with defaults a (10)\n\n";

    EXPECT_EQ(10u, a1.size());
    EXPECT_EQ(10, std::distance(begin(a1), end(a1)));
    EXPECT_EQ(10, std::distance(cbegin(a1), cend(a1)));

    /*
    EXPECT_PRED2(equalWithIterators, tenAs, a1);
    EXPECT_PRED2(equalWithGet, tenAs, a1);
    EXPECT_PRED2(equalWithSubscript, tenAs, a1);
    */
    for(size_t i = 0; i < a1.size(); i++) {
        EXPECT_EQ('a', a1[i]);
    }
    //Testing defaults for a2
    // std::cout << "Testing defaults for a2 with defaults b (20)\n\n";

    EXPECT_EQ(20u, a2.size());
    EXPECT_EQ(20, std::distance(begin(a2), end(a2)));
    EXPECT_EQ(20, std::distance(cbegin(a2), cend(a2)));

    /*
    EXPECT_PRED2(equalWithIterators, twentyBs, a2);
    EXPECT_PRED2(equalWithGet, twentyBs, a2);
    EXPECT_PRED2(equalWithSubscript, twentyBs, a2);
    */

    for(size_t i = 0; i < a2.size(); i++) {
        EXPECT_EQ('b', a2[i]);
    }
    //Testing defaults for a3
    // std::cout << "Testing defaults for a3 with defaults c (30)\n\n";

    EXPECT_EQ(30u, a3.size());
    EXPECT_EQ(30, std::distance(begin(a3), end(a3)));
    EXPECT_EQ(30, std::distance(cbegin(a3), cend(a3)));

    /*
    EXPECT_PRED2(equalWithIterators, thirtyCs, a3);
    EXPECT_PRED2(equalWithGet, thirtyCs, a3);
    EXPECT_PRED2(equalWithSubscript, thirtyCs, a3);
    */


    for(size_t i = 0; i < a3.size(); i++) {
        EXPECT_EQ('c', a3[i]);
    }
}

TEST(Constructor, Resize) {
    ARRAY a1 (10, 'a'), a2 (20, 'b'),a3 (30, 'c');

    a1.set ('d',19);
    a2.set ('e',29);
    a3.set ('f',39);

    // std::cout << "\n-----Performing resize tests via set-----\n\n";

    // std::cout << "Testing defaults for a1 with defaults a (set 19=d) (20)\n\n";

    EXPECT_EQ(20u, a1.size());
    EXPECT_EQ(20, std::distance(begin(a1), end(a1)));
    EXPECT_EQ(20, std::distance(cbegin(a1), cend(a1)));

    for (size_t i = 0; i < a1.size(); i++) {
        if(19u == i) {
            EXPECT_EQ('d', a1[i]);
        }
        else {
            EXPECT_EQ('a', a1[i]);
        }
    }

    //    std::cout << "Testing defaults for a2 with defaults a (set 29=e) (30)\n\n";

    EXPECT_EQ(30u, a2.size());
    EXPECT_EQ(30, std::distance(begin(a2), end(a2)));
    EXPECT_EQ(30, std::distance(cbegin(a2), cend(a2)));

    for (size_t i = 0; i < a2.size(); i++) {
        if(29u == i) {
            EXPECT_EQ('e', a2[i]);
        }
        else {
            EXPECT_EQ('b', a2[i]);
        }
    }

    //    std::cout << "Testing defaults for a3 with defaults a (set 39=f) (40)\n\n";

    EXPECT_EQ(40u, a3.size());
    EXPECT_EQ(40, std::distance(begin(a3), end(a3)));
    EXPECT_EQ(40, std::distance(cbegin(a3), cend(a3)));

    for (size_t i = 0; i < a3.size(); i++) {
        if(39u == i) {
            EXPECT_EQ('f', a3[i]);
        }
        else {
            EXPECT_EQ('c', a3[i]);
        }
    }

    // std::cout << "\n----- Default values have been tested. Please review results.-----\n\n";

}

TEST(Constructor, Size) {
    ARRAY a(10), b(10), c(9);
    EXPECT_EQ(10u, a.size());
    EXPECT_EQ(10, std::distance(begin(a), end(a)));
    EXPECT_EQ(10, std::distance(cbegin(a), cend(a)));
}

TEST(Constructor, SizeAndValue) {
    ARRAY a(10, 'a'), b(10, 'a'), c(10, 'b'), d(9, 'a');
    EXPECT_EQ(10u, a.size());
    EXPECT_EQ(10, std::distance(begin(a), end(a)));
    EXPECT_EQ(10, std::distance(cbegin(a), cend(a)));
    EXPECT_PRED2(equalWithIterators, tenAs, a);
    EXPECT_PRED2(equalWithGet, tenAs, a);
    EXPECT_PRED2(equalWithSubscript, tenAs, a);
    EXPECT_EQ(a, b);
    EXPECT_NE(a, c);
    EXPECT_NE(a, d);
    EXPECT_NE(c, d);
}
